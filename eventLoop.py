import pygame
from pygame.locals import *
import time
from sharedData.timedEvents import *
from sharedData.EventHandler import *
from spriteData.textElement import *
from spriteData.sprite import *
from imageUtils import *
import sharedData.types as types


MOVESPEED_PIXEL_PER_SECOND = 5
def runEventLoop(gameBoard):

    timeExecutive = TimeExecutive()
    gameBoard.redrawActiveRegion() # draw everything for the first time
    eventhandler = eventHandler()  # setup our event handler
    timeSinceKeyRegistered = time.time()
    while 1:
        currentTime = time.time()
        #only check for events 10 times a second

        if (currentTime - timeSinceKeyRegistered) > .01:
            timeSinceKeyRegistered = currentTime
            timeExecutive.executeTimers(currentTime)
            for event in pygame.event.get():
                if event.type == QUIT:
                    return

                # if we got a keyup event
                if event.type == KEYUP:

                    if event.key == K_SPACE:
                        gameBoard.doAction(types.ACTION_SPACE_RELEASED)

                    if keys[pygame.K_SLASH]:
                        print "slash released"
                        gameBoard.doAction(types.ACTION_RSHIFT_RELEASED)

                    if event.key == K_ESCAPE:
                        gameBoard.toggleMenu()

                    if event.key == K_t:
                        gameBoard.doDebugAction() # hook up commands to test to the 't' key

                if event.type == pygame.MOUSEBUTTONDOWN:
                     x, y = event.pos
                     gameBoard.click(x,y)

            # if nothing important happened, just get whatever keys the user pressed
            keys = pygame.key.get_pressed()
            if keys[pygame.K_w]:
                gameBoard.moveActiveSprite(0, -MOVESPEED_PIXEL_PER_SECOND)

            if keys[pygame.K_a]:
                gameBoard.moveActiveSprite(-MOVESPEED_PIXEL_PER_SECOND, 0)

            if keys[pygame.K_s]:
                gameBoard.moveActiveSprite(0, +MOVESPEED_PIXEL_PER_SECOND)

            if keys[pygame.K_d]:
                gameBoard.moveActiveSprite(+MOVESPEED_PIXEL_PER_SECOND, 0)

            if keys[pygame.K_SPACE]:
                gameBoard.doAction(types.ACTION_SPACE_PRESSED)

            if keys[pygame.K_SLASH]:
                print "slash pressed"
                gameBoard.doAction(types.ACTION_RSHIFT_PRESSED)

            gameBoard.refreshActiveRegion()

            #todo replace these events with an event object
            # process any events which might have come up
            event = eventhandler.getEvent()

            if event:
                eventindex, eventdata, sender = event
                if eventdata.startswith(types.EVENT_TELEPORT):
                    gameBoard.switchActiveRegion(eventdata.split()[1])

                if eventdata.startswith(types.EVENT_SCORE):
                    print eventdata

                if eventdata.startswith(types.EVENT_DRAW_TEMPORARY_TEXT):
                    #solution from https://stackoverflow.com/questions/3368969/find-string-between-two-substrings
                    text = eventdata.split("(")[1].split(")")[0]
                    xlocation, ylocation = eventdata.split(")")[1].split()

                    unclePomade, _ = load_png("unclePomade.png")
                    newSprite = Sprite(unclePomade, 0, 0, "unclePomade2", True)
                    newTextSprite = TextElement(text,int(xlocation),int(ylocation), "new text elem")
                    gameBoard.drawElementOnRegion(sender, newTextSprite)
                    timeExecutive.addTimer(Timer(gameBoard.eraseElementOnRegion, (sender, newTextSprite), currentTime, 1))

                if eventdata.startswith(types.EVENT_DELAYED_ERASE):
                    id, timeToErase = eventdata.split("(")[1].split(")")[0].split()
                    timeExecutive.addTimer(Timer(gameBoard.eraseElementOnRegion, (sender, id), currentTime, int(timeToErase)))

                if eventdata.startswith(types.EVENT_DIALOGUE):
                    text = eventdata.split("(")[1].split(")")[0].split()
                    text = string.join(text)
                    print text
                    gameBoard.dialogue(text)

                if eventdata.startswith(types.EVENT_END_DIALOGUE):
                    gameBoard.endDialogue()