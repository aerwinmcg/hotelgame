#!/usr/bin/python

from pygame.locals import *

from imageUtils import *
from mapData.map import *
from mapData.world import *
from sharedData.EventHandler import *
from spriteData.sprite import *

MOVESPEED = 2

def initGame():
    # Initialise screen

    eventhandler = eventHandler()
    pygame.init()
    screen = pygame.display.set_mode((256, 192))
    pygame.display.set_caption('Test Game')


    # Fill background
    background = pygame.Surface(screen.get_size())
    background = background.convert()

    player, _ = load_png("val.png")
    bed, _ = load_png("CountryBed.png")
    laundry, _ = load_png("LaundaryCart.png")
    door, _ = load_png("Door.png")
    playerLocation = Location(50, 50)
    playerSprite = Sprite(player, 0, 0, "val", False, True)
    backgroundMap, _ = load_png("ExampleBackground.png")

    outside = Map(backgroundMap, screen)
    outside.add(playerSprite)
    outside.add(Sprite(laundry, 200, 50, "Laundary", True, False))
    outside.add(Sprite(door, 100,100, "Door", False, False, ("Teleport", "inside") ))


    gameBoard = World()
    gameBoard.addRegion(outside,"outside", True)

    room, _ = load_png("Room2.png")


    inside = Map(room, screen)

    inside.add(Sprite(player, 200, 100, "val", False, True))
    #inside.add(Sprite(player, 200, 100, "unclePomade", False, True))
    inside.add(Sprite(door, 0,0, "door",False, False,("Teleport", "outside")))

    bedSprite = Sprite(bed, 50, 50, "Bed", False, False)
    messyBed, _ = load_png("messybed.png")
    bedSprite.addImageStack(messyBed)

    messyBedSprite = Sprite(messyBed, 50,50, "messybed", False, False)
    messyBedSprite.addImageStack(bed)
    inside.add(messyBedSprite)
    gameBoard.addRegion(inside, "inside", False)
    pygame.display.flip()

    menubg, _ = load_png("menu.png")
    menu = Map(menubg, screen)
    gameBoard.addRegion(menu, "menu", False)

    return background, gameBoard, eventhandler

def main():
    background, gameBoard, eventhandler = initGame()


    # Event loop
    while 1:
        for event in pygame.event.get():
            if event.type == QUIT:
                return

            if event.type == KEYUP:
                    if event.key == K_SPACE:
                        gameBoard.doAction("Space")
                    if event.key == K_m:
                        gameBoard.toggleMenu()
        ##first get keys
        keys = pygame.key.get_pressed()

        if keys[pygame.K_w]:
            gameBoard.moveActiveSprite(0, -MOVESPEED)
        if keys[pygame.K_a]:
            gameBoard.moveActiveSprite(-MOVESPEED, 0)
        if keys[pygame.K_s]:
            gameBoard.moveActiveSprite(0, +MOVESPEED)
        if keys[pygame.K_d]:
            gameBoard.moveActiveSprite(+MOVESPEED, 0)

        event = eventhandler.getEvent()

        if event:
                eventindex, eventdata = event
                if eventdata.startswith("Teleport"):
                    gameBoard.switchActiveRegion(eventdata.split()[1])

        background.fill((100, 100, 100))
        gameBoard.refreshActiveRegion()


#if __name__ == '__main__': main()