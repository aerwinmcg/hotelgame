#singleton pattern from http://python-3-patterns-idioms-test.readthedocs.io/en/latest/Singleton.html

from MapSupport.World import *
from MapSupport.map import *
from sprite.sprite import *
from sprite.commonSprites import *
from GameExecutiveSupport.automation import *
import GameExecutiveSupport.commonsequences as commonSequences


#Todo this is horrible and messy. It's been quarantined to this file, which is something to be thankful for.

class ScreenData:
    class __ScreenData:
        def __init__(self):
            return
    instance = None

    def __init__(self):
        if not ScreenData.instance:
            ScreenData.instance = ScreenData.__ScreenData()

    def __getattr__(self, name):
        return getattr(self.instance, name)

    world = World()
    eventhandler = eventHandler()
    inventory = Inventory()
    automation = Automation(world)

    def populateExterior(self):

        #todo could we automate this with better titles of image files?

        outside, _ = load_png("outside2.png")
        print outside
        print self.screen
        outsideMap = Map(types.MAP_NAME_EXTERIOR,outside, self.screen)

        #rightFence
        outsideMap.blockArea(800, 0, 1000, 680)

        #leftFence
        outsideMap.blockArea(0, 0, 350, 680)

        #buildingleft
        outsideMap.blockArea(350,0, 530, 640)
        #buildingRight
        outsideMap.blockArea(615,0, 800, 640)


        outsideMap.setAreaInteraction((531,0,614,640),
                                      (self.eventhandler.queueTeleportActiveSprite,
                                       (types.MAP_NAME_INTERIOR,
                                        types.LOCATION_TELEPORT_DESTINATION_INSIDE_IN_FRONT_OF_DOOR,
                                        outsideMap.name))
                                      )

        menuBar, _ = load_png("menuBar.png")
        outsideMap.addSprite(Sprite(menuBar, 0,0, drawLayer=10))
        outsideMap.addSprite(TextSprite("N/A",880,8, name_input = "Clock",refreshFunction=self.sharedMemory.getClockText, drawLevel=11))


        outsideMap.setAreaInteraction((1000,0,1024,1024),
                                      (self.eventhandler.queueTeleportActiveSprite,
                                       (types.MAP_NAME_STREET,
                                        types.LOCATION_TELEPORT_DESTINATION_STREET_LEFT,
                                        outsideMap.name))
                                      )
        return outsideMap



    def populateInterior(self):
        inside, _ = load_png("grossLobby.png")
        insideMap = Map(types.MAP_NAME_INTERIOR, inside, self.screen)



        menuBar, _ = load_png("menuBar.png")
        insideMap.addSprite(Sprite(menuBar, 0,0, drawLayer=10))
        insideMap.addSprite(TextSprite("N/A",880,8, name_input = "Clock",refreshFunction=self.sharedMemory.getClockText, drawLevel=11))

        #wall
        insideMap.blockArea(0,0,1024,64)

        #floorleft
        insideMap.blockArea(0,702,450,764)

        #floorright
        insideMap.blockArea(525,702,1024,764)


        insideMap.blockArea(615,0,615,800)



        #block to the left and right of the stairs
        insideMap.blockArea(445, 0, 445,109)
        insideMap.blockArea(530, 0, 530,109)


        #other items
        door, _ = load_png("doorway.png")
        doorSprite = Sprite(door, 470,750, interactions=[(types.SPRITE_ACTION_BUMP, self.eventhandler.queueTeleportActiveSprite,
                                                          [types.MAP_NAME_EXTERIOR,
                                                           types.LOCATION_TELEPORT_DESTINATION_OUTSIDE_IN_FRONT_OF_DOOR,
                                                           insideMap.name])])
        insideMap.addSprite(doorSprite)



        stairs, _ = load_png("grossStairs.png")
        stairsSprite = Sprite(stairs, 445,0, interactions=[(types.SPRITE_ACTION_BUMP,
                                             self.eventhandler.queueTeleportActiveSprite,
                                             [types.MAP_NAME_UPSTAIRS, types.LOCATION_TELEPORT_DESTINATION_UPSTAIRS_IN_FRONT_OF_STAIRS,
                                              insideMap.name])])


        insideMap.addSprite(stairsSprite)


        mediumTable, _ = load_png("mediumTable.png")
        mediumTableSprite = Sprite(mediumTable, 60, 250, drawLayer=1)
        insideMap.addSprite(mediumTableSprite)

        books, _ = load_png("books.png")
        booksSprite = Sprite(books, 65, 275, drawLayer=2)
        insideMap.addSprite(booksSprite)

        deskArea, _ = load_png("deskLampOff.png")
        #    def __init__(self, image_input, x_input, y_input, name_input=None, interactions = None, loopImages = True,endOfLoopAction = None, imageStack = None):
        deskAreaON, _ = load_png("deskLampOn.png")
        deskAreaSprite = LiveSprite(deskArea, 60, 325, imageStack =[deskAreaON], drawLayer=2,
                                    interactions=[(types.ACTION_USE_BUTTON_RELEASED, self.eventhandler.queueLampToggle, ["lamp"])])

        insideMap.addSprite(deskAreaSprite)

        waterJug, _ = load_png("fullWater.png")
        waterJugSprite = Sprite(waterJug, 80, 425, drawLayer=2)
        insideMap.addSprite(waterJugSprite)


        waterGlasses0, _ = load_png("glasses1Full.png")
        waterGlasses1, _ = load_png("glasses2Full.png")
        waterGlasses2, _ = load_png("glasses3Full.png")

        waterGlasses, _ = load_png("glasses0Full.png")
        waterGlassesSprite = generateHospitalitySprite(waterGlasses, 98,460,
                                                       [waterGlasses0,waterGlasses1,waterGlasses2])
        self.automation.registerSpriteAsHospitalityByID(waterGlassesSprite.name)
        insideMap.addSprite(waterGlassesSprite)


        flowersFresh, _ = load_png("flowerPotFull.png")
        flowersWilted, _ = load_png("flowerPotWilting.png")

        flowersSprite = generateHospitalitySprite(flowersWilted, 370, 395,
                                                       [flowersFresh])
        self.automation.registerSpriteAsHospitalityByID(flowersSprite.name)
        flowersSprite.preRequisiteItem = types.INVENTORY_ITEM_COSMOS
        insideMap.addSprite(flowersSprite)


        smallTable, _ = load_png("smallTable.png")
        smallTableSprite = Sprite(smallTable, 350,330)
        insideMap.addSprite(smallTableSprite)

        grossLamp, _ = load_png("grossLamp.png")
        grossLampSprite = Sprite(grossLamp, 355,330, drawLayer=2)
        insideMap.addSprite(grossLampSprite)


        sleepSprite = Sprite(smallTable, 500,500,interactions=
                             [(types.ACTION_USE_BUTTON_RELEASED, self.eventhandler.queueSleepEvent, ["example"])])

        insideMap.addSprite(sleepSprite)

        playerSprite = generateDefaultPlayerSprite(30,500)
        insideMap.addSprite(playerSprite)
        insideMap.setActiveSprite(playerSprite.name)


        return insideMap

    def populateUpstairs(self):

        upstairs, _ = load_png("grossUpstairs.png")
        upstairsMap = Map(types.MAP_NAME_UPSTAIRS, upstairs, self.screen)

        menuBar, _ = load_png("menuBar.png")
        upstairsMap.addSprite(Sprite(menuBar, 0,0, drawLayer=10))
        upstairsMap.addSprite(TextSprite("N/A",880,8, name_input = "Clock",refreshFunction=self.sharedMemory.getClockText, drawLevel=11))


        #player
        #playerSprite = generateDefaultPlayerSprite(20, 600)
        #upstairsMap.addSprite(playerSprite)
        #upstairsMap.setActiveSprite(playerSprite.name)

        #other items

        stairs,_ = load_png("grossStairs.png")
        stairsSprite = Sprite(stairs, 492,750,name_input="stairs",
                              interactions=[(types.SPRITE_ACTION_BUMP,
                                             self.eventhandler.queueTeleportActiveSprite,
                                                          [types.MAP_NAME_INTERIOR,types.INSIDE_LOCATION_STAIRS,upstairsMap.name])])
        bed, _ = load_png("bigBed.png")
        messyBed, _ = load_png("messyBigBed.png")

        messyBedSprite1 = generateBedSprite(30, 40, name="messybed1")
        self.automation.registerSpriteAsHygieneSpriteByID(messyBedSprite1.name, 0)

        messyBedSprite2 = generateBedSprite(300, 40,name="messybed2")
        self.automation.registerSpriteAsHygieneSpriteByID(messyBedSprite2.name,1)
        messyBedSprite2.nextImage(force = True)
        messyBedSprite3 = generateBedSprite(550, 40, name="messybed3")
        self.automation.registerSpriteAsHygieneSpriteByID(messyBedSprite3.name,2)
        messyBedSprite3.nextImage(force = True)

        messyBedSprite4 = generateBedSprite(815, 40,name="messybed4")
        self.automation.registerSpriteAsHygieneSpriteByID(messyBedSprite3.name,3)

        messyBedSprite4.nextImage(force = True)


        cart, _ = load_png("laundryCart.png")
        cartSprite = Sprite(cart,  4,314, name_input = "cart", weight=types.WEIGHT_NORMAL)

        cartSprite.interactions = [(types.ACTION_USE_BUTTON_RELEASED, self.inventory.addInventoryItem,
                                            [types.INVENTORY_ITEM_SHEET])]
        upstairsMap.addSprite(cartSprite)

        upstairsMap.addSprite(messyBedSprite1)
        upstairsMap.addSprite(messyBedSprite2)
        upstairsMap.addSprite(messyBedSprite3)
        upstairsMap.addSprite(messyBedSprite4)
        upstairsMap.addSprite(stairsSprite)


        #wall
        upstairsMap.blockArea(0,0,1024,64)

        #floorleft
        upstairsMap.blockArea(0,702,500,764)

        # floorright
        upstairsMap.blockArea(565, 702, 1024, 764)

        #wall1
        upstairsMap.blockArea(0,240,200,310)

        #wall2
        upstairsMap.blockArea(260,240,460,310)

        #wall3
        upstairsMap.blockArea(525,240,725,310)

        #wall4
        upstairsMap.blockArea(790,240,970,310)


        #verticalwall1
        upstairsMap.blockArea(258,0,265,245)

        #verticalwall2
        upstairsMap.blockArea(523,0,530,245)

        #verticalwall3
        upstairsMap.blockArea(788,0,795,245)

        guests = commonSprites.generateAllGuests()
        room = 0
        for g in guests:
            upstairsMap.addSprite(g)
            self.automation.registerSpriteAsGuestByID(g.name, room)
            room+=1

        #unclePomade, _ = load_png("unclePomade.png")

        #unclePomadeGuest1 = Sprite(unclePomade, 102,170,name_input = "pomade",speed=.5)
        #upstairsMap.addSprite(unclePomadeGuest1)


        #unclePomadeGuest2 = Sprite(unclePomade,317,204,speed=.5)
        #upstairsMap.addSprite(unclePomadeGuest2)
        #self.automation.registerSpriteAsGuestByID(unclePomadeGuest2.name, 1)

        #unclePomadeGuest3 = Sprite(unclePomade,685,185, name_input = "otherPomade",speed=.5)
        #unclePomadeGuest3 = Sprite(unclePomade,685,185, name_input = "otherPomade",speed=.5)
        #upstairsMap.addSprite(unclePomadeGuest3)
        #self.automation.registerSpriteAsGuestByID(unclePomadeGuest3.name, 2)
        #self.automation.registerSpriteAsGuestByID(unclePomadeGuest1.name, 0)


        return upstairsMap

    def populateMenuScreen(self):
        menuimage, _ = load_png("MenuBig.png")
        menuMap = MenuMap(types.MAP_NAME_ESC_MENU, menuimage, self.screen)

        menuMap.addSprite(TextSprite("Name - Val", 150, 200, name_input="val"))
        menuMap.addSprite(TextSprite(self.sharedMemory.getMoneyText(),150,265,name_input = "moneyText", refreshFunction=self.sharedMemory.getMoneyText))
        menuMap.addSprite(TextSprite(self.sharedMemory.getScoreText(),150,330, name_input = "scoreText",refreshFunction=self.sharedMemory.getScoreText))
        menuMap.addSprite(TextSprite(self.sharedMemory.getHygieneText(),150,395,name_input = "cleanText", refreshFunction=self.sharedMemory.getHygieneText))
        menuMap.addSprite(TextSprite(self.sharedMemory.getHospitalityText(),150,455,name_input = "hospitality", refreshFunction=self.sharedMemory.getHospitalityText))

        menuMap.addSprite(TextSprite("Menu",450,50,fontSize=types.FONT_TYPE_BIG, name_input="menutext"))
        menuMap.addSprite(TextSprite("N/A",769,142, name_input = "Clock",refreshFunction=self.sharedMemory.getClockText, drawLevel=11))

        menuMap.addSprite(TextSprite("Press \"esc\" to return",425, 660, name_input="escape text", drawLevel=11))

        okImage,_ = load_png("okButton.png")
        menuMap.addSprite(Sprite(okImage, 920,670, interactions=([(types.ACTION_CLICK, self.world.toggleMenu, [])]) ) )



        return menuMap


    def populateInventoryScreen(self):
        menuimage, _ = load_png("blankMenu.png")
        menuMap = InventoryMap(types.MAP_NAME_INVENTORY_MENU, menuimage, self.screen)

        handsImage,_ = load_png("handsSquare.png")
        x,y  = types.INVENTORY_HELD_ITEM_XY
        menuMap.addSprite(Sprite(handsImage, x,y,  name_input="hands"))

        okButton,_ = load_png("okButton.png")
        menuMap.addSprite(Sprite(okButton, 550, 175, interactions=[(types.ACTION_CLICK,sharedData.inventory.dropHeldItem, [])]))

        inventorySlots = commonSprites.generateArrayOfInventorySlots(types.NR_OF_INVENTORY_SLOTS)
        for i in inventorySlots:
            menuMap.addSprite(i)

        menuMap.addSprite(TextSprite("Inventory",400,50,fontSize=types.FONT_TYPE_BIG, name_input="menutext"))
        menuMap.addSprite(TextSprite("Press \"i\" to return",400, 660, name_input="escape text"))



        #okImage,_ = load_png("okButton.png")
        #menuMap.addSprite(Sprite(okImage, 920,670, interactions=([(types.ACTION_CLICK, self.world.toggleMenu, [])]) ) )
        return menuMap




    def populateStreet(self):

        #todo could we automate this with better titles of image files?

        street, _ = load_png("street.png")
        streetMap = Map(types.MAP_NAME_STREET, street, self.screen)
        streetMap.blockArea(0, 0, 1024, 690)


        menuBar, _ = load_png("menuBar.png")
        streetMap.addSprite(Sprite(menuBar, 0,0, drawLayer=10))
        streetMap.addSprite(TextSprite("N/A",880,8, name_input = "Clock",refreshFunction=self.sharedMemory.getClockText, drawLevel=11))

        #playerSprite = generateDefaultPlayerSprite(30,700)
        #streetMap.addSprite(playerSprite)
        #streetMap.setActiveSprite(playerSprite.name)

        streetMap.setAreaInteraction((0,680,10,900),
                                     (self.eventhandler.queueTeleportActiveSprite,
                                      (types.MAP_NAME_EXTERIOR,
                                       types.LOCATION_TELEPORT_DESTINATION_OUTSIDE_RIGHT,
                                       streetMap.name))
                                     )


        streetMap.setAreaInteraction((990,680,1000,900),
                                     (self.eventhandler.queueTeleportActiveSprite,
                                      (types.MAP_NAME_FACTORY,
                                       types.LOCATION_TELEPORT_DESTINATION_STREET_LEFT,
                                       streetMap.name))
                                     )

        return streetMap


    def populateFactory(self):

        #todo could we automate this with better titles of image files?

        factory, _ = load_png("factory.png")
        factoryMap = Map(types.MAP_NAME_FACTORY, factory, self.screen)
        factoryMap.blockArea(0, 0, 1024, 690)


        menuBar, _ = load_png("menuBar.png")
        factoryMap.addSprite(Sprite(menuBar, 0,0, drawLayer=10))
        factoryMap.addSprite(TextSprite("N/A",880,8, name_input = "Clock",refreshFunction=self.sharedMemory.getClockText, drawLevel=11))


        cosmosPlant, _ = load_png("cosmos.png")
        cosmos = Sprite(cosmosPlant, 770,640)
        cosmos.interactions.append((types.ACTION_USE_BUTTON_RELEASED, sharedData.inventory.addInventoryItem, [types.INVENTORY_ITEM_COSMOS]))
        factoryMap.addSprite(cosmos)


        factoryMap.setAreaInteraction((0,680,10,900),
                                     (self.eventhandler.queueTeleportActiveSprite,
                                      (types.MAP_NAME_STREET,
                                       types.LOCATION_TELEPORT_DESTINATION_STREET_RIGHT,
                                       factoryMap.name))
                                     )

        return factoryMap


    def setupScreen (self):
        pygame.init()
        self.screen = pygame.display.set_mode((1024, 768))
        self.automation.setScreen(self.screen)
        pygame.display.set_caption("Hotel")

        self.sharedMemory = SharedGameMemory()
        self.sharedMemory.initialize()

        pygame.font.init()  # you have to call this at the start,
        # if you want to use this module.


        # Fill background
        background = pygame.Surface(self.screen.get_size())
        background = background.convert()


        self.world.addMap(self.populateInterior())
        self.world.addMap(self.populateFactory())
        self.world.addMap(self.populateUpstairs())
        self.world.addMap(self.populateExterior())
        self.world.addMap(self.populateStreet())
        self.world.addMap(self.populateMenuScreen())
        self.world.addMap(self.populateInventoryScreen())
        self.sharedMemory.resetDailyValues()
        return self.world

