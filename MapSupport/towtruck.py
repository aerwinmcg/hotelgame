import sharedData.types as types
from pygame.rect import *
import sharedData.utilities as utilities
import math
class TowTruck:
    def  __init__(self):
        self.movementAllowed = True
        self.demographer = None
        self.cartographer = None
        self.movementSpeed = 5

    def assignDemographer(self, demographer):
        self.demographer = demographer

    def assignCartographer(self, cartographer):
        self.cartographer = cartographer

    def movementToXY(self,direction):
        x = 0
        y = 0
        if direction == types.DIRECTION_UP:
            y = -self.movementSpeed
        elif direction == types.DIRECTION_DOWN:
            y = self.movementSpeed
        elif direction == types.DIRECTION_LEFT:
            x = -self.movementSpeed
        elif direction == types.DIRECTION_RIGHT:
            x = self.movementSpeed
        return x,y

    #todo refactor this
    def moveActiveSprite(self, direction):
        if self.movementAllowed:
            activeSprite = self.demographer.getActiveSprite()
            x,y = self.movementToXY(direction)

            newSpriteRect = self.demographer.getRectAroundPlayerAndGrippedItem().move(x,y)
            self.demographer.animatePlayerSpriteByDirection(direction)

            self.cartographer.doInteractionForRect(newSpriteRect)
            if self.cartographer.isSpriteInFreeRegion(newSpriteRect):
                bumpedItems = self.demographer.getAllConnectedSpritesFromSprite(activeSprite, offsetx=x, offsety=y)
                if not bumpedItems:
                    movingWeight = self.demographer.getGrippedSpriteWeight()

                    xmove = self.generateWeightedSpeed(x, movingWeight)
                    ymove = self.generateWeightedSpeed(y, movingWeight)

                    self.demographer.movePlayer((xmove, ymove), relative=True)
                else:
                    movementPossible = True
                    for b in bumpedItems:
                        if not self.cartographer.isSpriteInFreeRegion(Rect(b.getRectangle()).move(x,y)):
                            movementPossible = False

                    if movementPossible:
                        movingWeight = self.demographer.getWeightOfSprites(bumpedItems) + self.demographer.getGrippedSpriteWeight()
                        if movingWeight < types.WEIGHT_IMMOVABLE:

                            xmove = self.generateWeightedSpeed(x,movingWeight)
                            ymove = self.generateWeightedSpeed(y,movingWeight)

                            for b in bumpedItems:
                                self.demographer.moveSprite(b, (xmove, ymove), relative=True)
                                self.demographer.movePlayer((xmove, ymove), relative=True)
                        else:
                            self.demographer.movePlayer((0, 0), relative=True)
                    else:
                        self.demographer.movePlayer((0, 0), relative=True)

                    newSpriteLocation = self.demographer.getRectAroundPlayer().move(x,y)
                    self.demographer.executeBumpActions(newSpriteLocation)
            else:
                self.demographer.movePlayer((0,0), relative=True)



    def moveNonPlayerSpriteByDirection(self, spriteID, direction):
        sprite = self.demographer.getSpriteByName(spriteID)
        x, y = self.movementToXY(direction)
        self.moveNonPlayerSpriteByXY(sprite, (x,y))

    def moveNonPlayerSpriteByXY(self, sprite, (x,y)):
        speed = sprite.speed
        if self.movementAllowed and ((not self.demographer.getGrippedSprite()) or (sprite.name != self.demographer.getGrippedSprite().name)):
            newSpriteRect = self.demographer.getRectangleAroundSprite(sprite).move(x*speed,y*speed)
            if self.cartographer.isSpriteInFreeRegion(newSpriteRect):
                bumpedItems = self.demographer.getSpritesInRect(newSpriteRect, sprite)
                if not bumpedItems:
                    self.demographer.moveSprite(sprite, (x*speed, y*speed), relative=True)

    def moveSpriteTowardsPoint(self, sprite, (x,y)):
        currentX, currentY = sprite.getLocation()
        xclose = utilities.isPrettyClose(currentX,x)
        yclose = utilities.isPrettyClose(currentY,y)
        if not xclose:
            newX = x-currentX
            if newX>0:
                self.moveNonPlayerSpriteByDirection(sprite.name, types.DIRECTION_RIGHT)
            else:
                self.moveNonPlayerSpriteByDirection(sprite.name, types.DIRECTION_LEFT)

        if not yclose:
            newY = y-currentY
            if newY > 0:
                self.moveNonPlayerSpriteByDirection(sprite.name, types.DIRECTION_DOWN)
            else:
                self.moveNonPlayerSpriteByDirection(sprite.name, types.DIRECTION_UP)

        if xclose and yclose:
            return True

        else:
            return False

    def moveSpriteSpeech(self, sprite,x,y, isAbsoluteLocation=False):
        speech = self.getSpriteByName(sprite.speech)
        self.moveSpriteByAmount(speech,x,y,isAbsoluteLocation)
        self.moveSpriteByAmount(sprite, 0,0)
        
    def disableMovement(self):
        self.movementAllowed = False

    def enableMovement(self):
        self.movementAllowed = True

    def generateWeightedSpeed(self, movement,weight):
        if weight >0:
            movement = float(movement)
            weight = float(weight)
            return int(math.ceil(movement/weight))
        else:
            return movement