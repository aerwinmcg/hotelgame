import sharedData.types as types
from sharedData.EventHandler import *

class World:
    def __init__(self):
        self.maps = dict()
        self.activeMap = None
        self.previousActiveMap = None
        self.eventhandler = eventHandler()
        self.eventhandler.subscribeForEvent(types.EVENT_CLOCK_TIME_ELAPSED, self.updateTextOnActiveMap)
        self.eventhandler.subscribeForEvent(types.EVENT_REDRAW_SCREEN_IN_BACKGROUND, self.drawScreenInBackground)
        self.eventhandler.subscribeForEvent(types.EVENT_REDRAW_SCREEN, self.redrawActiveMap)

    def drawScreenInBackground(self, data):
        self.activeMap.redrawScreenInBackground()

    def addMap(self,map):
        if self.activeMap == None:
            self.activeMap = map
        self.maps[map.name] = map

    def setActiveMap(self, name):
        self.activeMap = self.getMapByName(name)

    def deleteMap(self, map):
        del self.maps[map.name]

    def refreshActiveMap(self):
        self.activeMap.refreshScreen()

    def redrawActiveMap(self, data=None):
        self.activeMap.redrawScreen()

    def handleUserCommands(self, commands):
        self.activeMap.handleusercommands(commands)

    def click(self, x, y, clickBool):
        self.activeMap.handleClickAction(x, y, clickBool)

    def getMapByName(self, name):
        return self.maps[name]

    def startDialogue(self,text):
        self.activeMap.startDialogue(text)

    def endDialogue(self):
        self.activeMap.endDialogue()

    def toggleMenu(self,name=None):
        #If we're already at a menu
        if self.activeMap.maptype == types.MAP_TYPE_MENU:
            if (name) and (name != self.activeMap.name):
                print "cannot switch to this menu"
            elif (not name) or name == self.activeMap.name:
                self.switchActiveMap(self.previousActiveMap.name)
                self.eventhandler.queueClockResume("world")
        #if we're not on a menu now
        else:
            menuMap = self.getMapByName(name)
            if menuMap:
                if menuMap.maptype == types.MAP_TYPE_MENU:
                    self.previousActiveMap = self.activeMap
                    self.switchActiveMap(name)
                    self.eventhandler.queueClockPaused("world")
                else:
                    print "map "+name+" is not a menu. doing nothing."

            else:
                print "no map found by that name"

        return

    def updateMousePosition(self, mouse):
        self.activeMap.updateMousePosition(mouse)

    def switchActiveMap(self, map):
        self.setActiveMap(map)
        self.activeMap.redrawScreen()

    def moveSpriteByDirectionById(self, ID, direction):
        for key, value in self.maps.iteritems():
            if value.hasSprite(ID):
                value.moveSpriteByDirection(ID, direction)

    def moveSpriteTowardsLocation(self, spriteID, location):
            map = self.maps[self.getMapForSprite(spriteID)]
            return map.moveSpriteTowardLocation(spriteID, location)

    def teleportSprite(self, spriteID, destinationMap, (x,y)):
        map = self.maps[self.getMapForSprite(spriteID)]
        newMap = self.maps[destinationMap]
        newMap.addSprite(map.getSprite(spriteID))
        print ("added")
        map.deleteSprite(spriteID)
        print ("deleted")
        newMap.setSpriteLocation(spriteID, (int(x),int(y)))
        print ("added")

    def teleportActiveSprite(self, x,y, destinationMap):
        
        if self.activeMap == self.maps[destinationMap]:
            return
        spriteID = self.activeMap.getActiveSprite()
        map = self.maps[self.getMapForSprite(spriteID)]
        map.setActiveSprite(None)
        
        self.teleportSprite(spriteID, destinationMap, (x,y))
        self.switchActiveMap(destinationMap)
        self.maps[destinationMap].setActiveSprite(spriteID)

    def getMapForSprite(self, spriteID):
        for key, value in self.maps.iteritems():
            if value.hasSprite(spriteID):
                return key

    def updateTextOnActiveMap(self,data):
        self.activeMap.updateText()

