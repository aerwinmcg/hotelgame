import pygame

from sharedData.EventHandler import *
import sharedData.utilities as utilities
from sharedData.sharedGameData import *
from pygame.rect import *
class Cartographer:
    def __init__(self, screen):
        '''
        self.background = background_input

        self.screen = screen_input
        self.spriteElements = []
        self.shapeElements = []
        self.textElements = []
        self.eventhandler = eventHandler()
        self.blockedAreas = []
        self.sharedMemory = SharedGameMemory()
        self.activeSprite = None
        self.grippedObject = None
        self.name = name
        self.mapType = types.MAP_TYPE_DEFAULT
        self.movementDisabled = False
        '''

        self.backgroundBlockZones = []
        self.backgroundInteractiveZone = []
        self.demographer = None
        self.screen = screen
        self.sharedMemory = SharedGameMemory()

    #setup functions*********************************************

    def assignDemographer (self, demographer):
        self.demographer = demographer

    def blockArea(self,x1,y1,x2,y2):
        self.backgroundBlockZones.append((x1,y1,x2-x1,y2-y1))

    def setInteractiveArea(self, (x1,y1,x2,y2), interaction):
        self.backgroundInteractiveZone.append(((x1,y1,x2-x1,y2-y1), interaction))

    def doSpace(self):
        pass
    def doShift(self):
        pass
    def doUnShift(self):
        pass
    def doUnSpace(self):
        pass
    def unGripObject(self):
        pass


    #utility functions / checks ****************************************************


    #I don't know why this isn't provided automatically... got this from
    #https://stackoverflow.com/questions/14099872/concatenating-two-range-function-results
    def union(self, a, b):
        if a and b:
            sortedA = sorted(a)
            sortedB = sorted(b)
            smallest = None
            biggest = None
            if sortedA[0] < sortedB[0]:
                smallest = sortedA[0]
            else:
                smallest = sortedB[0]
            if sortedA[-1] > sortedB[-1]:
                biggest = sortedA[-1]
            else:
                biggest = sortedB[-1]

            return range(smallest-1, biggest+1)
        else:
            return None


    def isOffScreen(self,(x,y,width,height)):

        #elif's don't do much because the return statements would already exit the block...
        if x <0:
            return True

        #block movement off top-screen
        elif y < 0:
            return True

        screenX, screenY = self.screen.get_size()

        #block movement off right of screen
        if x+width > screenX:
            return True

        #block movement off bottom of screen
        elif y+height > screenY:
            return True

        else:
            #return false if all checks are OK, we are still on-screen
            return False

    def isSpriteInFreeRegion(self, spriteRectangle):
            for b in self.backgroundBlockZones:
                if spriteRectangle.colliderect(Rect(b)):
                    return False
            if self.isOffScreen(spriteRectangle):
                return False
            return True

    def doInteractionForRect(self, rectangle):
        for zone, interaction in self.backgroundInteractiveZone:
            if rectangle.colliderect(Rect(zone)):
                method, args = interaction
                utilities.remoteExecuteFunction(method, args)

    #Screen redraw******************************************************************

    def delayedErase(self, id, duration=1):
        action = types.EVENT_DELAYED_ERASE
        data = "("+str(id) + " " + str(duration)+")"
        event = action +" "+ data
        self.eventhandler.addEvent(event, self.name)

    def updateDisplay(self):
        pygame.display.update()

    def triggerDialogue(self,text):
        pass

