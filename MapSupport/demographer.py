import sharedData.types as types
from sprite.sprite import *
from sharedData.utilities import *
from sharedData.sharedGameData import *
from sharedData.EventHandler import *
from pygame.rect import *
import sprite.commonSprites as commonSprites

# Demographer Class ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
class Demographer:
    def __init__(self):
        '''
        The demographer is responsible for knowing sprite properties, and updating them.
        '''

        self.sprites = dict()
        self.rectsNeedingRedraw = []
        self.activeSprite = None
        self.grippedSprite = None
        self.eventhandler = eventHandler()
        self.name = "Demographer"
        self.griprange = 10
        self.heldItem = None
        self.sharedMemory = SharedGameMemory()
        self.idsForSpritesWithAIs = []
        self.subscribeForEvents()

    def subscribeForEvents(self):
        self.eventhandler.subscribeForEvent(types.EVENT_GUEST_READY_FOR_CHECKOUT, self.setGuestForCheckout)
        self.eventhandler.subscribeForEvent(types.EVENT_DELETE_SPRITE, self.deleteSpriteFromEvent)
        self.eventhandler.subscribeForEvent(types.EVENT_RESET_SPRITE, self.resetSprite)

# Add / Delete sprites -------------------------------------------------------------------------------------------------

    def add(self, element_input):
        self.sprites[element_input.name] = element_input
        self.sprites[element_input.name].refreshNeeded = True
        if element_input.hasAI:
            print element_input.name + " has AI "
            self.idsForSpritesWithAIs.append(element_input.name)

    def delete(self, element_input):
        rect = self.getRectangleAroundSprite(element_input)
        self.rectsNeedingRedraw.append(rect)
        del self.sprites[element_input.name]

    def deleteByName(self, name):
        s = self.getSpriteByName(name)
        self.delete(s)

    def deleteSpriteFromEvent(self, data):
        event = data.split("(")[0]
        sprite = data.split("(")[1].split(")")[0]
        sender = data.split("(")[1].split(")")[1]

        try:
            sprite = self.sprites[sprite]

        except:
            return
        print "deleting "+sprite.name
        self.delete(sprite)

    def setActiveSprite(self, name):
        if name == None:
            self.activeSprite = None
        else:
            self.activeSprite = self.sprites[name]

# Get sprites ----------------------------------------------------------------------------------------------------------
    def getSpriteByName(self,name):
        return self.sprites.get(name)

    def getSpritesInRect(self, rectangle,ignoreSprite=None):
        spritesInRect = []

        for key, value in self.sprites.iteritems():
            if rectangle.colliderect(value.getRectangle()):
                if not ignoreSprite or (ignoreSprite and ignoreSprite.name != key):
                    spritesInRect.append(value)

        return spritesInRect

    def getNonPlayerSpritesInRect(self,rectangle):
        spritesInRect = []

        for key, value in self.sprites.iteritems():
            if rectangle.colliderect(value.getRectangle()):
                if value != self.activeSprite and value != self.grippedSprite:
                    spritesInRect.append(value)

        return spritesInRect

    def getGrippedSprite(self):
        return self.grippedSprite

    def getAllConnectedSpritesFromSprite(self,sprite, processedSprites = None, offsetx = 0, offsety = 0,
                                         ignoreOffsets = False):
        if not processedSprites:
            processedSprites = []

        spriteRect = self.getRectangleAroundSprite(sprite, ignoreOffsets)
        spriteRect = spriteRect.move(offsetx, offsety)

        touchingSprites = self.getNonPlayerSpritesInRect(spriteRect)

        for s in touchingSprites:
            if s not in processedSprites:
                processedSprites.append(s)
                processedSprites = self.getAllConnectedSpritesFromSprite(s, processedSprites, offsetx,offsety)

        return processedSprites

    def getAllSprites(self):
        allSprites = []
        for key, value in self.sprites.iteritems():
            allSprites.append(value)
        if self.heldItem:
            allSprites.append(self.heldItem)
        if self.activeSprite:
            allSprites.append(self.activeSprite)
        return allSprites


    def getActiveSprite(self):
        return self.activeSprite

# Action Support -------------------------------------------------------------------------------------------------------

    def startDialogue(self,text):

        id = "BG"
        bg_image, _ = load_png("dialogue.png")
        newElem = Sprite(bg_image, 0, 700, id)
        self.add(newElem)

        id = "OK"
        ok_image, _ = load_png("okButton.png")

        newElem = Sprite(ok_image, 950,739, id, ([(types.ACTION_CLICK, self.eventhandler.queueEndDialogue, [self.name])]) )
        self.add(newElem)

        x, y = 40, 710
        id = "Dialogue"
        newElem = TextSprite(text, x, y, id, fontSize=types.FONT_TYPE_BIG)
        self.add(newElem)

    def endDialogue(self):
        self.deleteByName("BG")
        self.deleteByName("OK")
        self.deleteByName("Dialogue")

    def doAction(self,action):
        if action == types.ACTION_SPACE_PRESSED and not self.grippedSprite:
            self.grip()

        elif action==types.ACTION_SPACE_RELEASED and self.grippedSprite:
            self.ungrip()

        #this shouldn't happen, but we need a safeguard against clicking because it's handled below in "click()"
        if action!=types.ACTION_CLICK:
            nearbySprites = self.getSpritesInRect(self.getRectAroundPlayer().inflate(70,70), ignoreSprite=self.activeSprite)
            for s in nearbySprites:
                print s.name
                s.interact(action)

    def handleMouseMovement(self, mouse):
        pass

    def click(self, x, y, clickBool):
        if clickBool:
            for key, value in self.sprites.iteritems():
                rect = pygame.Rect(value.getRectangle())
                if rect.collidepoint(x, y):
                    value.interact(types.ACTION_CLICK)
                    print "tried to click on "+key

    def executeBumpActions(self, rectangle):
        for sprite in self.getSpritesInRect(rectangle, ignoreSprite=self.activeSprite):
            sprite.interact(types.SPRITE_ACTION_BUMP)


# Rectangle support ----------------------------------------------------------------------------------------------------
    def getRectAroundPlayer(self, ignoreOffset=False):
        if ignoreOffset:
            return Rect(self.activeSprite.getRectangle())
        elif self.activeSprite:
            rect = Rect(self.activeSprite.getRectangle())
            rect.top +=30
            rect.height -=30
            return rect
        else:
            print "no active sprite found"
            return None

    def getRectAroundPlayerAndGrippedItem(self, ignoreOffset=False):
        if self.grippedSprite:
            spriteRect = self.getRectAroundPlayer(ignoreOffset)
            return spriteRect.union(Rect(self.grippedSprite.getRectangle()))
        else:
            return self.getRectAroundPlayer(ignoreOffset)

    def getRectangleAroundSprite(self, sprite, raw=False):
        if sprite == self.activeSprite:
            return self.getRectAroundPlayerAndGrippedItem(raw)
        else:
            return Rect(sprite.getRectangle())

# Dirty Sprite behavior ------------------------------------------------------------------------------------------------
    def markSpritesInRectDirty(self, rect):
        for s in self.getSpritesInRect(rect):
            s.refreshNeeded = True

    def markAllSpritesDirty(self):
        for key,value in self.sprites.iteritems():
            value.refreshNeeded = True

    def getDirtySprites(self):
        if self.activeSprite:
            self.refreshHeldItem()
        dirtySprites = []
        rects = self.rectsNeedingRedraw
        for key, value in self.sprites.iteritems():
            if value.refreshNeeded == True:
                dirtySprites.append(value)
                value.setRefreshNeeded(False)
        if self.heldItem and self.heldItem.refreshNeeded == True:
            dirtySprites.append(self.heldItem)
            dirtySprites.append(self.activeSprite)
            self.heldItem.setRefreshNeeded(False)
        self.rectsNeedingRedraw = []
        dirtySprites = self.processDirtySprites(dirtySprites)
        return dirtySprites, rects

    def processDirtySprites(self, initialDirtySprites):
        superset = initialDirtySprites
        for dirty in initialDirtySprites:
            superset = superset + self.getAllConnectedSpritesFromSprite(dirty, ignoreOffsets=True)
        return list(set(superset))


    def refreshHeldItem(self):
        heldItem = self.sharedMemory.getHeldItem()

        #no change because neither is out of sync
        if heldItem == types.INVENTORY_EMPTY_ITEM and not self.heldItem:
            return

        x, y = self.activeSprite.getLocation()

        #if we need to start holding an item
        if heldItem != types.INVENTORY_EMPTY_ITEM and not self.heldItem:
            #put arms up
            newSprite = commonSprites.generateDefaultPlayerSpriteWithArmsUp(x, y)
            newSprite.direction=self.activeSprite.direction
            newSprite.updateImage()
            self.deleteByName(self.activeSprite.name)
            self.add(newSprite)
            self.setActiveSprite(newSprite.name)

            #make item
            y -= 20
            self.heldItem = commonSprites.generateInventoryItemByName(heldItem, x, y)
            self.heldItem.refreshNeeded = True

        elif self.heldItem and heldItem != self.heldItem.name and heldItem!= types.INVENTORY_EMPTY_ITEM:
            y -= 20
            self.rectsNeedingRedraw.append(self.getRectangleAroundSprite(self.heldItem))
            self.heldItem = commonSprites.generateInventoryItemByName(heldItem, x, y)
            self.heldItem.refreshNeeded = True


        elif heldItem == types.INVENTORY_EMPTY_ITEM and self.heldItem:
            # put arms down
            newSprite = commonSprites.generateDefaultPlayerSprite(x, y)
            newSprite.direction=self.activeSprite.direction
            print newSprite.direction
            newSprite.updateImage()
            self.deleteByName(self.activeSprite.name)
            self.add(newSprite)
            self.setActiveSprite(newSprite.name)

            self.rectsNeedingRedraw.append(self.getRectangleAroundSprite(self.heldItem))
            self.heldItem = None

    def updateText(self):
        for key, value in self.sprites.iteritems():
            if value.type == types.ELEMENT_TYPE_TEXT:
                value.refreshNeeded = True
                self.markSpritesInRectDirty(self.getRectangleAroundSprite(value))


# Grip support ---------------------------------------------------------------------------------------------------------
    def grip(self):
        rect = Rect(self.activeSprite.getRectangle()).inflate(self.griprange, self.griprange)
        sprites = self.getSpritesInRect(rect)
        sprites = filter(lambda a: a != self.activeSprite, sprites)
        if sprites and sprites[0].weight < types.WEIGHT_IMMOVABLE:
            self.grippedSprite = sprites[0]

    def ungrip(self):
        self.grippedSprite = None

    def getGrippedSpriteWeight(self):
        weight = 0
        if self.grippedSprite:
            weight = self.grippedSprite.weight
        return weight

    def moveGrippedSprite(self, location, relative):
        if self.grippedSprite:
            self.moveSprite(self.grippedSprite, location, relative)

# Sprite Movement/Animation support ------------------------------------------------------------------------------------
    def moveSprite(self,sprite,location,relative):
        if sprite:
            self.rectsNeedingRedraw.append(self.getRectangleAroundSprite(sprite, True))
            sprite.setLocation(location,relative)
            sprite.refreshNeeded = True
            for s in self.getAllConnectedSpritesFromSprite(sprite, ignoreOffsets=True):
                s.refreshNeeded = True

    def updateSpriteByDirection(self, sprite, direction):
        if sprite.type == types.ELEMENT_TYPE_MOVING_SPRITE:
            sprite.setDirection(direction)
            sprite.updateImage()
            sprite.refreshNeeded = True
        else:
            print "error! the sprite "+sprite.name+ " is not a moving sprite, so we can't change its direction!"

    def animatePlayerSpriteByDirection(self, direction):
        self.updateSpriteByDirection(self.activeSprite, direction)
        self.activeSprite.updateImage()

    def getWeightOfSprites(self, sprites):
        weight = 0
        for s in sprites:
            weight += s.weight
        return weight

    def movePlayer(self,location, relative):
        self.moveSprite(self.activeSprite, location, relative)
        self.moveGrippedSprite(location, relative)
        self.moveSprite(self.heldItem, location, relative)

    def resetSprite(self, data):
        event = data.split("(")[0]
        spriteID = data.split("(")[1].split(")")[0]
        sender = data.split("(")[1].split(")")[1]
        try:
            sprite = self.sprites[spriteID]

        except:
            print "cannot find the guest for " + spriteID
            return

        sprite.reset()

# Guest support --------------------------------------------------------------------------------------------------------
    def setGuestForCheckout(self,data):
        event = data.split("(")[0]
        guestID = data.split("(")[1].split(")")[0]
        sender = data.split("(")[1].split(")")[1]

        try:
            sprite = self.sprites[guestID]

        except:
            print "cannot find the guest for " + guestID
            return

        sprite.interactions.append(( types.ACTION_CLICK, self.checkIfGuestCanBeCheckedOut, [guestID]) )

    def checkIfGuestCanBeCheckedOut(self, guestname):
        self.sharedMemory.incrementMoneyByRoomPrice()
        self.eventhandler.queueGuestIsDoneCheckingOut(guestname, "demographer")


# Inventory Support Class ---------------------------------------------------------------------------------------------
class InventoryDemographer(Demographer):
    def __init__(self):
        Demographer.__init__(self)
        self.selectedItem = None
        self.selectedItemRectangle = None
        self.inventoryHelper = InventoryHelper(self)
        self.inventorySprites = self.inventoryHelper.getInventoryAsSprites()
        self.mouseIsClicked = False
        self.draggedItem = None
        self.originalDraggedItemSlot = None
        self.eventhandler = eventHandler()
        self.eventhandler.subscribeForEvent(types.EVENT_INVENTORY_CHANGED, self.updateInventoryScreen)

    def handleMouseMovement(self, mouse):
        x,y = mouse
        if self.draggedItem:
            self.moveSprite(self.draggedItem, (x,y), False)

    def click(self, x, y, clickBool):
        self.mouseIsClicked = clickBool

        inventorySlot = self.inventoryHelper.getInventorySlotByXY(x, y)

        print ["inventory slot is: ", inventorySlot]
        print [clickBool, self.draggedItem]
        if not clickBool and self.draggedItem:
            if inventorySlot:
                print "original inventory slot:"
                print self.originalDraggedItemSlot

                print "new inventory slot:"
                print inventorySlot

                self.sharedMemory.inventory.swapItemsBySlot(self.originalDraggedItemSlot, inventorySlot)
            self.updateInventoryScreen(None)
            self.draggedItem = None
            self.originalDraggedItemSlot = None
            self.eventhandler.queueRedrawScreenEvent("inventoryHandler")

        #click down
        else:
            for key, value in self.inventorySprites.iteritems():
                rect = pygame.Rect(value.getRectangle())
                if rect.collidepoint(x, y):
                    print "dragging "+value.name
                    self.draggedItem = value
                    self.originalDraggedItemSlot = inventorySlot
                    self.eventhandler.queueRedrawScreenEvent("inventoryHandler")

        for key, value in self.sprites.iteritems():
            rect = pygame.Rect(value.getRectangle())
            if rect.collidepoint(x, y):
                value.interact(types.ACTION_CLICK)

    def updateInventoryScreen(self, data):
        self.markAllSpritesDirty()
        for key, value in self.inventorySprites.iteritems():
            value.refreshNeeded = True
        '''
        if self.inventorySprites:
            for key, value in self.inventorySprites.iteritems():
                dirtyRectangle = self.getRectangleAroundSprite(value)
                self.rectsNeedingRedraw.append(dirtyRectangle)
                redraw = self.getSpritesInRect(dirtyRectangle)
                for r in redraw:
                    r.refreshNeeded = True
        '''
        self.inventorySprites = self.inventoryHelper.getInventoryAsSprites()

    def getDirtySprites(self):
        dirtySprites = []
        rects = self.rectsNeedingRedraw
        self.rectsNeedingRedraw = []
        if self.inventorySprites:
            for key, value in self.inventorySprites.iteritems():
                if value.refreshNeeded == True:
                    dirtySprites.append(value)
                    value.setRefreshNeeded(False)
        for key, value in self.sprites.iteritems():
            if value.refreshNeeded == True:
                print key
                dirtySprites.append(value)
                value.setRefreshNeeded(False)
        return dirtySprites, rects

    def getAllSprites(self):
        allSprites = []
        self.inventoryHelper.refreshInventory()
        if self.inventorySprites:
            for key, value in self.inventorySprites.iteritems():
                allSprites.append(value)
        return allSprites+Demographer.getAllSprites(self)

class InventoryHelper:
    def __init__(self, demographerInstance):
        self.sharedMemory = SharedGameMemory()
        self.demographerInstance = demographerInstance
        self.inventoryLocationForHeldItem = (500,170)

    def getInventorySlotByXY(self,x,y):
        for i in range (0, types.NR_OF_INVENTORY_SLOTS):
            rect = self.getInventoryRectByInventoryNumber(i)
            if rect.collidepoint(x,y):
                print [i, "inventory slot"]
                return i
        else:
            rect = InventoryDemographer.getSpriteByName(self.demographerInstance,"hands").getRectangle()
            if rect.collidepoint(x,y):
                print "heldItem"
                return types.INVENTORY_SLOT_HELD
        return None

    def getXYInventoryPositionByInventoryNumber(self, number):
        name = types.SEQUENTIAL_LIST_OF_INVENTORY_SLOTS[number]
        sprite = InventoryDemographer.getSpriteByName(self.demographerInstance, name)
        if sprite:
            x,y = sprite.getLocation()
        else:
            x,y = None, None
        return x,y

    def getInventoryRectByInventoryNumber(self, number):
        name = types.SEQUENTIAL_LIST_OF_INVENTORY_SLOTS[number]
        sprite = InventoryDemographer.getSpriteByName(self.demographerInstance, name)
        rect = sprite.getRectangle()
        return rect


    def getInventoryPositionByName(self,name):
        index = 0
        for i in types.SEQUENTIAL_LIST_OF_INVENTORY_SLOTS:
            if i == name:
                return index
            index+=1
        return None

    def getInventoryAsSprites(self):
        print "getting inventory request"
        inventory = self.refreshInventory()
        return inventory

    def refreshInventory(self):
        print "refreshing inventory"
        items = self.sharedMemory.inventory.getAllItemsInInventory()
        heldItem = self.sharedMemory.getHeldItem()
        print heldItem
        index = 0
        inventorySprites = dict()
        for i in items:
            x,y = self.getXYInventoryPositionByInventoryNumber(index)
            if i != types.INVENTORY_EMPTY_ITEM:
                    newSprite = commonSprites.generateInventoryItemByName(i, x, y)
                    newSprite.refreshNeeded = True
                    inventorySprites[newSprite.name]=newSprite
            index += 1

        if heldItem != types.INVENTORY_EMPTY_ITEM:
            print "found held item named "+heldItem
            x,y = self.inventoryLocationForHeldItem
            newSprite = commonSprites.generateInventoryItemByName(heldItem, x, y)
            newSprite.refreshNeeded = True
            inventorySprites[newSprite.name] = newSprite

        return inventorySprites
