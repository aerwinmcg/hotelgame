from MapSupport.cartographer import *
from MapSupport.demographer  import *
from MapSupport.towtruck     import *
from ScreenSupport.picasso   import *
from sharedData.sharedGameData import *

class Map:
    def __init__(self, name, background, screen):
        self.name = name
        self.cartographer = Cartographer(screen)
        self.demographer = Demographer()
        self.towtruck = TowTruck()
        self.picasso = Picasso(screen, background)
        self.background = background
        self.maptype = types.MAP_TYPE_DEFAULT
        self.dialogueOngoing = False
        self.sharedData = SharedGameMemory()

        self.towtruck.assignDemographer(self.demographer)
        self.towtruck.assignCartographer(self.cartographer)
        self.cartographer.assignDemographer(self.demographer)

    def blockArea(self,x1, y1, x2, y2):
        self.cartographer.blockArea(x1, y1, x2, y2)

    def setAreaInteraction(self, area,interaction):
        self.cartographer.setInteractiveArea(area,interaction)

    def updateText(self):
        self.demographer.updateText()

    def handleusercommands(self, commands):
        for c in commands:
            if c in types.DIRECTIONS:
                self.towtruck.moveActiveSprite(c)
            if c in types.ACTIONS:
                self.demographer.doAction(c)

    def refreshScreen(self):
        spritesToRedraw, rectsToRedraw = self.demographer.getDirtySprites()
        if spritesToRedraw or rectsToRedraw:
            self.picasso.drawSprites(spritesToRedraw,rectsToRedraw)

    def redrawScreen(self):
        spritesToRedraw = self.demographer.getAllSprites()
        self.picasso.drawEverything(spritesToRedraw, self.background)

    def redrawScreenInBackground(self):
        spritesToRedraw = self.demographer.getAllSprites()
        self.picasso.drawEverything(spritesToRedraw, self.background, updateScreen =False)


    def startDialogue(self, text):
        if not self.dialogueOngoing:
            self.demographer.startDialogue(text)
            self.towtruck.disableMovement()
            self.dialogueOngoing = True

    def endDialogue(self):
        if self.dialogueOngoing:
            self.demographer.endDialogue()
            self.towtruck.enableMovement()
            self.dialogueOngoing = False

    def hasSprite(self, spriteID):
        sprite = self.demographer.getSpriteByName(spriteID)
        if sprite:
            return True
        else:
            return False

    def moveSpriteByDirection(self, spriteName, direction):
        self.towtruck.moveNonPlayerSpriteByDirection(spriteName, direction)

    def setActiveSprite(self, spriteName):
        self.demographer.setActiveSprite(spriteName)

    def handleClickAction(self,x, y, clickBool):
        self.demographer.click(x, y, clickBool)

    def updateMousePosition(self, mouse):
        self.demographer.handleMouseMovement(mouse)

    def addSprite(self,sprite):
        self.demographer.add(sprite)

    def getSprite(self, spriteID):
        return self.demographer.getSpriteByName(spriteID)

    def deleteSprite(self, spriteID):
        self.demographer.deleteByName(spriteID)

    def setSpriteLocation(self, spriteID, location):
        self.demographer.moveSprite(self.demographer.getSpriteByName(spriteID), location, relative=False)

    def moveSpriteTowardLocation(self, spriteID, location):
        return self.towtruck.moveSpriteTowardsPoint(self.demographer.getSpriteByName(spriteID), location)


    def getActiveSprite(self):
        return self.demographer.getActiveSprite().name



class MenuMap(Map):
    def __init__(self, name, background, screen):
        Map.__init__(self,name,background, screen)
        self.maptype = types.MAP_TYPE_MENU

    #stub out non-click actions
    def handleusercommands(self,commands):
        print "got command"
        pass

class InventoryMap(MenuMap):
    def __init__(self, name, background, screen):
        MenuMap.__init__(self,name,background, screen)
        self.demographer = InventoryDemographer()

    def redrawScreen(self):
        print "redrawing inventoryscreen"
        self.demographer.selectedItem = None
        self.demographer.selectedItemRectangle = None
        spritesToRedraw = self.demographer.getAllSprites()
        self.picasso.drawEverything(spritesToRedraw, self.background)

    #stub out non-click actions
    def handleusercommands(self,commands):
        for c in commands:
            if c in types.ACTIONS:
                self.demographer.doAction(c)