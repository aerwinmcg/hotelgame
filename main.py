#!/usr/bin/python

from GameExecutiveSupport.gameExecutive import *
from screenData import *
import pygame
import thread

def main():

    #get sprites and map setup
    pygame.font.init()  # you have to call this at the start,
    # if you want to use this module.
    s = ScreenData()
    world = s.setupScreen()
    #Start running everything
    GameExecutive(world).run()

if __name__ == '__main__': main()