from sprite import *
from sharedData.utilities import *
from sharedData.sharedGameData import *
from GameExecutiveSupport.automation import *

sharedData = SharedGameMemory()

def generateDefaultPlayerSprite(x_location, y_location):
        player, _ = load_png("playerMediumDown0.png")
        playerDown, _ = load_png("playerMediumDown0.png")
        playerUp, _ = load_png("playerMediumUp0.png")
        playerLeft, _ = load_png("playerMediumLeft0.png")
        playerRight, _ = load_png("playerMediumRight0.png")

        right1, _ = load_png("playerMediumRight1.png")
        right2, _ = load_png("playerMediumRight2.png")
        left1, _ = load_png("playerMediumLeft1.png")
        left2, _ = load_png("playerMediumLeft2.png")
        up1, _ = load_png("playerMediumUp1.png")
        up2, _ = load_png("playerMediumUp2.png")
        down1, _ = load_png("playerMediumDown1.png")
        down2, _ = load_png("playerMediumDown2.png")

        upList = [playerUp, up1, playerUp, up2]
        downList = [playerDown, down1, playerDown, down2]
        leftList = [playerLeft, left1, playerLeft, left2]
        rightList = [playerRight, right1, playerRight, right2]

        name = "Val"
        return MovingSprite(x_location, y_location, upList, downList, leftList, rightList,  name, drawLevel = 10)


def generateDefaultPlayerSpriteWithArmsUp(x_location, y_location):
    player, _ = load_png("playerMediumDown0.png")
    playerDown, _ = load_png("playerDownArms0.png")
    playerUp, _ = load_png("playerUpArms0.png")
    playerLeft, _ = load_png("playerLeftArms0.png")
    playerRight, _ = load_png("playerRightArms0.png")

    right1, _ = load_png("playerRightArms1.png")
    right2, _ = load_png("playerRightArms2.png")
    left1, _ = load_png("playerLeftArms1.png")
    left2, _ = load_png("playerLeftArms2.png")
    up1, _ = load_png("playerUpArms1.png")
    up2, _ = load_png("playerUpArms2.png")
    down1, _ = load_png("playerDownArms1.png")
    down2, _ = load_png("playerDownArms2.png")

    upList = [playerUp, up1, playerUp, up2]
    downList = [playerDown, down1, playerDown, down2]
    leftList = [playerLeft, left1, playerLeft, left2]
    rightList = [playerRight, right1, playerRight, right2]

    name = "Val arms up"
    return MovingSprite(x_location, y_location, upList, downList, leftList, rightList, name, drawLevel=10)


def generateBedSprite(xlocation,ylocation,name=None, drawlayer=2):
    bed, _ = load_png("bigBed.png")
    messyBed, _ = load_png("messyBigBed.png")
    return LiveSprite(messyBed, xlocation, ylocation, imageStack=[bed], loopImages = False,
                      endOfLoopAction=(sharedData.incrementHygiene),
                        drawLayer=drawlayer, preRequesiteItem=types.INVENTORY_ITEM_SHEET, name_input = name)

def generateCleanableSprite(image, xlocation,ylocation, imagestack, name=None, drawlayer=2):
    return LiveSprite(image, xlocation, ylocation, imageStack=imagestack, loopImages = False,
                      endOfLoopAction=(sharedData.incrementHygiene),
                        drawLayer=drawlayer)

def generateHospitalitySprite(image, xlocation,ylocation, imagestack, name=None, drawlayer=2):
    sprite = LiveSprite(image, xlocation,ylocation, imageStack=imagestack, loopImages = False,
                      endOfLoopAction=(sharedData.incrementHospitality),
                        drawLayer=drawlayer)
    return sprite



def generateAllGuests():
    unclePomade, _ = load_png("unclePomade.png")
    guests = []
    numberOfGuests = sharedData.generateNumberOfGuestsByPopularity()
    for x in range(0,numberOfGuests):
        guests.append(generateGuestSprite(unclePomade, x))
    return guests


def generateGuestSprite(image, roomNumber, name=None):
    x,y = utilities.generateRandomPointInRect(types.SEQUENTIAL_LIST_OF_RECTANGLES_AROUND_ROOMS[roomNumber])
    return Sprite(image, x,y, name_input=name, speed=.5)


def generateInventoryItemByName(name,x,y,spriteName=None):
    filename = None
    if name == types.INVENTORY_ITEM_POMADE:
        filename = "unclePomade.png"
    elif name == types.INVENTORY_ITEM_OK_BUTTON:
        filename = "okButton.png"
    elif name == types.INVENTORY_ITEM_SHEET:
        filename = "sheet.png"
    elif name == types.INVENTORY_ITEM_SPRAY:
        filename ="sprayBottle.png"
    elif name == types.INVENTORY_ITEM_COSMOS:
        filename ="cosmositem.png"

    if filename:
        image, _ = load_png(filename)
        return Sprite(image, x,y, drawLayer = types.DRAW_LAYER_INVENTORY_ITEM, name_input=spriteName)

def generateSelectedItemRectangle(x, y):
    name = "selectedItemRectangle.png"
    image,_ = load_png(name)
    return Sprite(image, x,y)

def generateArrayOfInventorySlots(numberOfSlots):
    slots = []
    inventorySquare, _ = load_png("inventorySquare.png")
    xcoord = 175
    ycoord = 250
    index = 0
    #need to update to support further rows
    for i in range(0, numberOfSlots):
        slots.append(Sprite(inventorySquare, xcoord, ycoord, name_input=types.SEQUENTIAL_LIST_OF_INVENTORY_SLOTS[index]))
        xcoord += 70
        index+=1

    return slots