import pygame
import sharedData.types as types
from sharedData.utilities import *
from sharedData.sharedGameData import *
from pygame.rect import *
import time
import copy


class Sprite:
    '''
    The sprite type is the base type for many sprite-types. It is a basic element on the screen which has a name, an
    image, and a location (x,y). It also supports interactions (clicking, bumping, space button, etc).
    '''
    def __init__(self, displayElement, x_input, y_input, name_input=None, interactions = None,
                 weight = types.WEIGHT_IMMOVABLE, drawLayer = 1, aiType = None, speed= 1):
        '''
        this is the constructor

        :param image_input: The image associated with the sprite
        :type image_input: pygame.image

        :param interactions: Optional parameter to assign interactions to this sprite
        :type interactions: [(Event type, function, [argument list])]


        '''

        self.sharedData = SharedGameMemory()
        self.weight = weight
        self.location = Location(x_input, y_input)
        self.displayElement = displayElement
        self.type = types.ELEMENT_TYPE_SPRITE
        if not name_input:
            self.name = getRandomString()
        else:
            self.name = name_input
        self.refreshNeeded = True
        self.interactions = interactions
        if not self.interactions:
            self.interactions = []
        self.drawLayer = drawLayer
        if aiType:
            self.setAIType(aiType)
            self.hasAI = True
        else:
            self.hasAI = False
        self.speed = speed


    def setRespawnPoint(self, respawnPoint = None):
        '''
        Sets the point at which a sprite will be respawned on map switch. If not specified, uses the current location.
        If this function is not called, there will be no respawn of the sprite.

        :param respawnPoint: The respawn location
        :type respawnPoint: Location
        :return: None

        '''

        if respawnPoint:
            self.respawnPoint = respawnPoint
        else:
            self.respawnPoint = self.location.getLocation()

    def getLocation(self):
        return self.location.getLocation()

    #todo remove this, it's just re-implementing some pygame stuff
    def getRectangle(self):
        x,y = self.getLocation()
        width,height = self.displayElement.get_size()

        r = Rect(x, y, width, height)
        return Rect(x, y, width, height)

    def setLocation (self, location, relative=False):
        ''':
        Set Location:

        Sets the location of a specified sprite. Location input is an x/y tuple, Relative flag indicates whether the
        sprite location should be set relative to the current position, or be an absolute position (default).

        '''
        if relative:
            x, y = location
            currentX, currentY = self.getLocation()
            self.setLocation((x + currentX, y + currentY))
        else:
            self.location.setLocation(location)

    def getSize(self):
        return self.displayElement.get_size()

    def interact(self, interactionType):
        '''Interact:

        Takes an interaction type, and looks to see if a function/arguments are registered for that interaction. If so,
        it is executed.

        This can handle multiple functions being assigned for a given action.
        '''

        if interactionType == types.ACTION_CLICK:
            print self.name + ": LOCATION "+str(self.getLocation())
        if self.interactions:
            for i in self.interactions:
                interaction, method, args = i
                if interaction == interactionType:
                    print "found interaction "+interaction
                    remoteExecuteFunction(method, args)

    def setRefreshNeeded(self, refreshNeeded):
        self.refreshNeeded = refreshNeeded

    def setAIType(self,ai):
        self.sharedData.ekumen.subscribeForUniqueAI(self.name, ai)

    def reset(self):
        pass

class LiveSprite(Sprite):
    def __init__(self, image_input, x_input, y_input, name_input=None, drawLayer = 1, interactions = None, loopImages = True,
                 endOfLoopAction = None, imageStack = None, preRequesiteItem=None,aiType=None,speed= 1):

        #initialize the parent object first
        Sprite.__init__(self,image_input,x_input,y_input,name_input,interactions=interactions, drawLayer =drawLayer, aiType=aiType, speed=speed)

        #just handle what we need to extend for this object
        self.loopImages = loopImages
        self.endOfLoopAction = endOfLoopAction
        self.imageStack = imageStack
        self.type = types.ELEMENT_TYPE_LIVE_SPRITE
        self.originalImage = copy.copy(self.displayElement)
        self.originalImageStack = copy.copy(self.imageStack)
        self.staleImageStack = []
        self.preRequisiteItem = preRequesiteItem
        if self.interactions:
            self.interactions.append((types.ACTION_USE_BUTTON_RELEASED, self.nextImage, []))
        else:
            self.interactions = [(types.ACTION_USE_BUTTON_RELEASED, self.nextImage, [])]


    def nextImage(self, force = False):
        if self.imageStack:
            if not force:
                if self.preRequisiteItem and not(self.sharedData.inventory.useItemIfPossible(self.preRequisiteItem)):
                    return
            refreshNeeded = False

            self.staleImageStack.append(self.displayElement)
            self.displayElement = self.imageStack.pop(0)
            self.refreshNeeded = True
            if not self.imageStack and self.staleImageStack:
                # execute any end-of-image actions we might need
                if self.endOfLoopAction:
                    remoteExecuteFunction(self.endOfLoopAction)

        # if we have reached the end of the loop, no more fresh images, only stale images
        elif self.staleImageStack:
            # loop the images if needed
            if self.loopImages:
                self.imageStack.append(self.displayElement)
                if self.staleImageStack:
                    self.displayElement = self.staleImageStack.pop(0)
                if self.staleImageStack:
                    self.imageStack = self.imageStack + self.staleImageStack
                self.staleImageStack = []
                self.refreshNeeded = True

    def resetImage(self):
        self.displayElement = self.originalImage
        self.imageStack = copy.copy(self.originalImageStack)
        self.staleImageStack = []

    def reset(self):
        self.resetImage()

class MovingSprite(Sprite):
    def __init__(self, x_input, y_input, upImageStack, downImageStack, leftImageStack, rightImageStack,
                 name_input=None, interactions = None, direction=types.DIRECTION_DOWN, drawLevel = 1, aiType = None, speed=1):

        #initialize the parent object first
        Sprite.__init__(self, downImageStack[0],x_input,y_input,name_input,interactions, drawLayer=drawLevel, aiType=aiType, speed=speed)

        #just handle what we need to extend for this object
        self.upImageStack = upImageStack
        self.downImageStack = downImageStack
        self.leftImageStack = leftImageStack
        self.rightImageStack = rightImageStack
        self.direction = direction
        self.type = types.ELEMENT_TYPE_MOVING_SPRITE
        self.lastUpdateTime = time.time()
        self.animationSpeed = .25  # animations per second
        self.updateImage()


    def setDirection(self, direction):
        self.direction = direction

    def updateImage(self):
        if ( time.time() - self.lastUpdateTime ) > self.animationSpeed:
            self.lastUpdateTime = time.time()
            if self.direction == types.DIRECTION_UP:
                self.updateImageFromStack(self.upImageStack)
            elif self.direction == types.DIRECTION_DOWN:
                self.updateImageFromStack(self.downImageStack)
            elif self.direction == types.DIRECTION_LEFT:
                self.updateImageFromStack(self.leftImageStack)
            elif self.direction == types.DIRECTION_RIGHT:
                self.updateImageFromStack(self.rightImageStack)
            else:
                print "Invalid direction! "+self.direction+ " setting to a nice default of DOWN!"
                self.setDirection(types.DIRECTION_DOWN)
                self.updateImageFromStack(self.downImageStack)

    def updateImageFromStack(self, stack):
        self.displayElement = stack[0]
        stack.append(stack[0])
        stack.pop(0)


class TextSprite(Sprite):

    def __init__(self, text_input, x_input, y_input, name_input=None, fontSize = types.FONT_TYPE_MEDIUM,
                 interactions = None, refreshFunction = None, drawLevel = 1):
        self.font = pygame.font.SysFont('Lucida Console', fontSize)
        text = self.font.render(str(text_input), False, (0, 0, 0))
        self.refreshFunction = refreshFunction
        Sprite.__init__(self,text,x_input,y_input,name_input,interactions, drawLayer=drawLevel)
        self.type = types.ELEMENT_TYPE_TEXT

    def refreshText(self):
        if self.refreshFunction:
            text = self.refreshFunction()
            self.displayElement = self.font.render(text, False, (0, 0, 0))

class Location:
    def __init__(self, x_input, y_input):
        self.x = x_input
        self.y = y_input

    def getLocation(self):
        return(self.x, self.y)

    def setLocation(self, (x, y)):
        self.x = x
        self.y = y

