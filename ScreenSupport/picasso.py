import sharedData.types as types
import pygame

class Picasso:
    def __init__(self, screen, background):
        self.screen = screen
        self.background = background

#draw===================================================================================================================


#got from https://stackoverflow.com/questions/2338531/python-sorting-a-list-of-objects
    def compareSpritesByDrawLayer(self, sprite1, sprite2):
        if sprite1.drawLayer > sprite2.drawLayer:
            return 1
        elif sprite1.drawLayer == sprite2.drawLayer:
            return 0
        else:  # x.resultType < y.resultType
            return -1


    def draw(self,element):
        pass

    def drawSprite(self, sprite):
        if sprite.type == types.ELEMENT_TYPE_TEXT:
            self.drawText(sprite)

        elif sprite.type == types.ELEMENT_TYPE_SHAPE:
            self.drawShape(sprite)

        elif sprite.type in types.SPRITE_TYPES:
            self.screen.blit(sprite.displayElement, sprite.getLocation())

    def drawText(self, text):
        text.refreshText()
        self.screen.blit(text.displayElement, text.getLocation())

    def drawShape(self, shape):
        if shape.shape == types.SHAPE_TYPE_RECTANGLE:
            x,y = shape.getLocation()
            pygame.draw.rect(self.screen, shape.color, (x, y, shape.width, shape.height), 0)

    def drawSprites(self, sprites, rects = None, updateScreen = True):
        if rects:
            for r in rects:
                left, top, width, height = r
                self.screen.blit(self.background, (left,top),(left, top, width, height))
        if sprites:
            sprites.sort(self.compareSpritesByDrawLayer)
            for s in sprites:
                self.eraseSprite(s)
            for s in sprites:
                self.drawSprite(s)
        if updateScreen:
            self.updateDisplay()

    def drawEverything(self, sprites, background, updateScreen = True):
        self.screen.blit(background, (0, 0))
        self.drawSprites(sprites, updateScreen=updateScreen)

    def reDrawText(self):
        for t in self.textElements:
            self.drawText(t)

    def updateDisplay(self):
        pygame.display.update()

#erase==================================================================================================================

    def erase(self, element):
        pass
        return

    def eraseSprite(self, sprite):
        x, y = sprite.getLocation()
        width,height = sprite.displayElement.get_size()
        self.screen.blit(self.background, (x, y), (x, y, width, height))

        #self.screen.blit(self.background, (x, y), (x, y, x+width, y+height))


