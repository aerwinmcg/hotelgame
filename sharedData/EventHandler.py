import sharedData.types as types

#borg pattern
class eventHandler:
    __shared_state = {}
    def __init__(self):
        self.__dict__ = self.__shared_state

    eventDict = dict()

    def addEvent(self, event, data):
        self.processEvent(event, data)

#todo fix all of these unsafe key error exception cases
    def subscribeForEvent(self, eventType, functionToCall):
        if self.eventDict.get(eventType):
            self.eventDict[eventType].append(functionToCall)
        else:
            self.eventDict[eventType] = [functionToCall]

    def processEvent(self, event, data):
        if self.eventDict[event]:
            for method in self.eventDict[event]:
                method(data)

#Helpers for queueing things=========================================================

    def queueDialogue(self,text,sender):
        event = types.EVENT_DIALOGUE
        data = event +" ("+text+")"
        data = data +" "+ sender
        self.addEvent(event, data)

    def queueEndDialogue(self,sender):
        self.sendEventWithEmptyData(sender, types.EVENT_END_DIALOGUE)

    def queueTeleportActiveSprite(self, teleportDestinationMap, (teleportDestinationX, teleportDestinationY), sender):
        event = types.EVENT_TELEPORT_ACTIVE_SPRITE
        data = event + " (" + str(teleportDestinationX) + " " + str(
            teleportDestinationY) + " " + teleportDestinationMap + ")"
        data = data + " " + sender
        self.addEvent(event,data)

    def queueInventoryChange(self, sender):
        self.sendEventWithEmptyData(sender, types.EVENT_INVENTORY_CHANGED)

    def sendEventWithEmptyData(self, sender, eventType):
        event = eventType
        data = event + " ()"
        data = data + " " + sender
        self.addEvent(event, data)

    def queueSpriteTeleport(self, teleportDestinationX,teleportDestinationY, teleportDestinationMap, spriteID, sender):
        event = types.EVENT_TELEPORT_SPRITE
        data = event+" ("+str(teleportDestinationX)+" " + str(teleportDestinationY) + " " + teleportDestinationMap + " " + spriteID+")"
        data = data + " " + sender
        self.addEvent(event, data)

    def queueClockUpdate(self, sender):
        self.sendEventWithEmptyData(sender, types.EVENT_CLOCK_TIME_ELAPSED)

    def queueHourEvent(self, hour, sender):
        event = types.EVENT_CLOCK_HOUR_ELAPSED
        data = event+" ("+str(hour)+")"
        data = data + " " + sender
        self.addEvent(event, data)

    def queueClockPaused(self, sender):
        self.sendEventWithEmptyData(sender, types.EVENT_CLOCK_PAUSED)

    def queueClockResume(self, sender):
        self.sendEventWithEmptyData(sender, types.EVENT_CLOCK_RESUME)

    def queueLampToggle(self, sender):
        self.sendEventWithEmptyData(sender, types.EVENT_LAMP_TOGGLE)

    def queueGuestReadyForCheckout(self, guestName, sender):
        event = types.EVENT_GUEST_READY_FOR_CHECKOUT
        data = event+" ("+str(guestName)+")"
        data = data + " " + sender
        self.addEvent(event, data)

    def queueGuestIsDoneCheckingOut(self, guestName, sender):
        event = types.EVENT_GUEST_DONE_CHECKING_OUT
        data = event+" ("+str(guestName)+")"
        data = data + " " + sender
        self.addEvent(event, data)

    def queueDeleteSprite(self, spriteID, sender):
        event = types.EVENT_DELETE_SPRITE
        data = event+" ("+str(spriteID)+")"
        data = data + " " + sender
        self.addEvent(event, data)

    def queueSleepEvent(self, sender):
        self.sendEventWithEmptyData(sender, types.EVENT_SLEEP)

    def queueRedrawScreenInBackgroundEvent(self, sender):
        self.sendEventWithEmptyData(sender, types.EVENT_REDRAW_SCREEN_IN_BACKGROUND)

    def queueRedrawScreenEvent(self, sender):
        self.sendEventWithEmptyData(sender, types.EVENT_REDRAW_SCREEN)


    def queueSpriteReset(self, sprite, sender):
        event = types.EVENT_RESET_SPRITE
        data = event+" ("+str(sprite)+")"
        data = data + " " + sender
        self.addEvent(event, data)