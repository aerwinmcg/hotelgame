import random
import string
import pygame
import types as types
import os

def getRandomString():
    return ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(10))

def remoteExecuteFunction(method, args=None):
    if method:
        if args:
            method(*args)
        else:
            method()

def remoteExecuteListOfFunction(func):
        method, args = func
        if args:
            remoteExecuteFunction(method, args)
        else:
            remoteExecuteFunction(method)

def isPrettyClose(x,y,closeAmount = 10):
    try:
        if abs(x-y) <= closeAmount:
            return True
        else:
            return False
    except:
        print [x, y]

def load_png(name):
    """ Load image and return image object"""
    fullname = os.path.join('data', name)
    try:
        image = pygame.image.load(fullname)
        if image.get_alpha() is None:
            image = image.convert()
        else:
            image = image.convert_alpha()
    except pygame.error, message:
        print 'Cannot load image:', fullname
        raise SystemExit, message
    return image, image.get_rect()

def getARandomDirection():
    directionIndex = random.randint(0, 3)
    return types.SEQUENTIAL_LIST_OF_DIRECTIONS[directionIndex]


def generateRandomPointInRect(rect):
    left, top, width, height= rect

    x = random.randint(left, left+width)
    y = random.randint(top, top+height)

    return (x,y)
