#using the borg singleton pattern
import pygame
from sharedData.EventHandler import *
import sharedData.utilities as utilities
import sharedData.types as types
import random
#todo this is the place where we could retrieve saved data...

class SharedGameMemory:
    __shared_state = {}
    def __init__(self):
        self.__dict__ = self.__shared_state

    def incrementScore(self, amount=1):
        self.score = self.score + amount

    def incrementMoney(self,amount=1):
        self.money = self.money + amount

    def incrementMoneyByRoomPrice(self):
        self.money +=self.roomRate

    def initialize(self):
        self.score = 0
        self.money = 0
        self.hospitality = 0
        self.hygiene = 0
        self.heldItem = None
        self.inventory = Inventory()
        self.eventhandler = eventHandler()
        self.inventory.initialize()
        self.clockHour = 8
        self.clockMinute = 55
        self.clockIsPaused = False
        self.eventhandler.subscribeForEvent(types.EVENT_CLOCK_PAUSED, self.pauseClock)
        self.eventhandler.subscribeForEvent(types.EVENT_CLOCK_RESUME, self.resumeClock)
        self.roomRate = 50
        self.guestsSupported = 4

    def updateScoresForDay(self):
        self.score = (self.hospitality+self.hygiene) / 2
        self.resetDailyValues()
        self.clockHour = 6
        self.clockMinute = 0
        self.updateClock()


    def pauseClock(self, data):
        self.clockIsPaused = True

    def resumeClock(self, data):
        self.clockIsPaused = False

    def getScoreText(self):
        return "Popularity - "+str(self.score)

    def getClockText(self):
        if self.clockMinute <10:
            minuteString = "0"+str(self.clockMinute)
        else:
            minuteString = str(self.clockMinute)

        return str(self.clockHour) + ":"+minuteString

    def getHygieneText(self):
        return "Hygiene - "+str(self.hygiene)

    def getMoneyText(self):
        return "Money - "+str(self.money)

    def getHospitalityText(self):
        return "Hospitality -"+str(self.hospitality)

    def getScore(self):
        return self.score

    def getMoney(self):
        return self.money

    def incrementHospitality(self, amount=1):
        self.hospitality += amount
        print "incrementing hospitality"

    def getHospitality(self):
        return self.hospitality

    def incrementHygiene(self, amount=1):
        self.hygiene+=amount
        print "incrementing hygiene to" + str(self.hygiene)

    def resetDailyValues(self):
        self.hospitality = 0
        self.hygiene = 0

    def getHeldItem(self):
        return self.inventory.getHeldItem()

    def updateClock(self):
        if not self.clockIsPaused:
            if self.clockMinute == 59:
                self.clockMinute = 0
                if self.clockHour == 12:
                    self.clockHour = 1
                else:
                    self.clockHour = self.clockHour+1
                    self.eventhandler.queueHourEvent(self.clockHour, "automation")

            else:
                self.clockMinute = self.clockMinute+1

            self.eventhandler.queueClockUpdate("sharedData")
        else:
            pass


    def generateNumberOfGuestsByPopularity(self):
        nrOfGuests = self.score
        if nrOfGuests > self.guestsSupported:
            nrOfGuests = self.guestsSupported
        return nrOfGuests


#this should never be too large, so this will be just a list. Later we can switch to a dictionary
class Inventory:
    __shared_state = {}
    def __init__(self):
        self.__dict__ = self.__shared_state
        self.eventhandler = eventHandler()

    def initialize(self):
        self.inventoryItems = []
        for i in range (0, int(types.NR_OF_INVENTORY_SLOTS)):
            print i
            self.inventoryItems.append(types.INVENTORY_EMPTY_ITEM)
        self.currentlyHeldItem = types.INVENTORY_EMPTY_ITEM

    def getAllItemsInInventory(self,returnHeldItem = False):
        returnedItems = self.inventoryItems
        if returnHeldItem:
            returnedItems.append(self.currentlyHeldItem)
        return returnedItems

    def getHeldItem(self):
        return self.currentlyHeldItem

    def switchHeldItemByName(self,name):
        self.eventhandler.addEvent(types.EVENT_INVENTORY_CHANGED, "inventoryManager")
        index = 0
        for i in self.inventoryItems:
            if i == name:
                self.currentlyHeldItem = i
                self.inventoryItems.pop(index)
            index+=1

    def switchHeldItemByRelativeIndex(self, index=1):
        self.inventoryItems.append(self.currentlyHeldItem)
        self.currentlyHeldItem = self.inventoryItems.pop(index)
        self.eventhandler.addEvent(types.EVENT_INVENTORY_CHANGED, "inventoryManager")
        print self.getAllItemsInInventory()

    def addInventoryItem(self,name, slot=None):
        print "trying to add item "+name
        if slot and slot == -1:
            self.currentlyHeldItem = name
        elif slot:
            self.inventoryItems[slot] = name
        else:
            if self.currentlyHeldItem == types.INVENTORY_EMPTY_ITEM:
                self.currentlyHeldItem = name
                print "no other item held. using main spot"
            else:
                print "trying to find a place for the item"
                for i in range (0, types.NR_OF_INVENTORY_SLOTS):
                    print "trying slot "+str(i)
                    print self.inventoryItems[i]
                    print name
                    if self.inventoryItems[i] == types.INVENTORY_EMPTY_ITEM and self.inventoryItems[i]!=name:
                        self.inventoryItems[i] = name
                        break

        self.eventhandler.addEvent(types.EVENT_INVENTORY_CHANGED, "inventoryManager")

    def goForwardInInventory(self):
        self.inventoryItems.insert(0,self.currentlyHeldItem)
        self.currentlyHeldItem = self.inventoryItems.pop()
        print self.getAllItemsInInventory()
        self.eventhandler.addEvent(types.EVENT_INVENTORY_CHANGED, "inventoryManager")

    def goBackwardInInventory(self):
        self.inventoryItems.append(self.currentlyHeldItem)
        self.currentlyHeldItem = self.inventoryItems.pop(0)
        self.eventhandler.addEvent(types.EVENT_INVENTORY_CHANGED, "inventoryManager")

    def swapItemsBySlot(self,slot1,slot2):
        print slot1
        print slot2

        if (slot1 == types.INVENTORY_SLOT_HELD):
            swap = self.inventoryItems[slot2]
            self.inventoryItems[slot2] = self.currentlyHeldItem
            self.currentlyHeldItem = swap

        elif (slot2 == types.INVENTORY_SLOT_HELD):
            swap = self.inventoryItems[slot1]
            self.inventoryItems[slot1] = self.currentlyHeldItem
            self.currentlyHeldItem = swap

        else:
            swap = self.inventoryItems[slot2]
            self.inventoryItems[slot2] = self.inventoryItems[slot1]
            self.inventoryItems[slot1] = swap
        self.eventhandler.addEvent(types.EVENT_INVENTORY_CHANGED, "inventoryManager")

    def swapHeldItemWithSlot(self,slot):
        swap = self.currentlyHeldItem
        self.currentlyHeldItem = self.inventoryItems[slot]
        self.inventoryItems[slot] = swap
        self.eventhandler.addEvent(types.EVENT_INVENTORY_CHANGED, "inventoryManager")

    def findItemByName(self,item):
        index = None
        if self.currentlyHeldItem==item:
            index = -1
        else:
            for i in range (0,types.NR_OF_INVENTORY_SLOTS):
                if self.inventoryItems[i] == item:
                    index = i

        return i

    def useItemIfPossible(self,item):
        used = False
        if self.currentlyHeldItem == item:
            self.currentlyHeldItem = types.INVENTORY_EMPTY_ITEM
            used = True
        else:
            used = False
        self.eventhandler.addEvent(types.EVENT_INVENTORY_CHANGED, "inventoryManager")
        return used

    def dropHeldItem(self):
        print "dropping held item"
        self.currentlyHeldItem = types.INVENTORY_EMPTY_ITEM
        self.eventhandler.addEvent(types.EVENT_INVENTORY_CHANGED, "inventoryManager")
