import time

class TimeExecutive:
    def __init__(self):
        self.timers = []

    def addTimer(self, timer):
        self.timers.append(timer)

    def executeTimers(self, currentTime = None):
        if currentTime == None:
            currentTime = time.time()
        i = 0
        for t in self.timers:
            if t.isItTimeToExecute(currentTime):
                t.executeMethod()
                self.timers.pop(i)
            i+=1

class Timer:
    def __init__(self, methodToBeCalled, argumentsForFunction, currentTime, secondsUntilCall, absoluteTime=False):
        self.method = methodToBeCalled
        self.args = argumentsForFunction
        if absoluteTime:
            self.timeToCall = secondsUntilCall
        else:
            self.timeToCall = currentTime + secondsUntilCall

    def isItTimeToExecute(self, currentTime):
        if currentTime - self.timeToCall > 0:
            return True
        else:
            return False

    def executeMethod(self):
        self.method(*self.args)
