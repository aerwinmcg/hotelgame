import pygame
import os
import random
import string
def load_png(name):
    """ Load image and return image object"""
    fullname = os.path.join('data', name)
    try:
        image = pygame.image.load(fullname)
        if image.get_alpha() is None:
            image = image.convert()
        else:
            image = image.convert_alpha()
    except pygame.error, message:
        print 'Cannot load image:', fullname
        raise SystemExit, message
    return image, image.get_rect()

#got from https://stackoverflow.com/questions/2257441/random-string-generation-with-upper-case-letters-and-digits-in-python


def getRandomString():
    return ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(10))