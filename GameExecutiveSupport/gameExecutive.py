import time
import pygame
import sharedData.types as types
from pygame.locals import *
from sharedData.timedEvents import *
from sharedData.EventHandler import *
from sharedData.sharedGameData import *
from automation import *
import string


class GameExecutive:
    def __init__(self,world):
        self.world = world
        self.timeExecutive = TimeExecutive()
        self.events = []
        self.subscribeForEvents()
        self.inventory = Inventory()

    def addToEventQueue(self, event):
        self.events.append(event)

    def subscribeForEvents(self):
        eventhandler = eventHandler()
        eventhandler.subscribeForEvent(types.EVENT_DIALOGUE, self.addToEventQueue)
        eventhandler.subscribeForEvent(types.EVENT_TELEPORT_ACTIVE_SPRITE, self.addToEventQueue)
        eventhandler.subscribeForEvent(types.EVENT_SCORE, self.addToEventQueue)
        eventhandler.subscribeForEvent(types.EVENT_DRAW_TEMPORARY_TEXT, self.addToEventQueue)
        eventhandler.subscribeForEvent(types.EVENT_END_DIALOGUE, self.addToEventQueue)
        eventhandler.subscribeForEvent(types.EVENT_DELAYED_ERASE, self.addToEventQueue)
        eventhandler.subscribeForEvent(types.EVENT_TELEPORT_SPRITE, self.addToEventQueue)

    def run(self):
        refreshNeeded = True
        timeSinceKeyRegistered = time.time()
        self.world.redrawActiveMap()
        automation = Automation(self.world)
        events = []
        while 1:
            currentTime = time.time()
            # only check for events 10 times a second
            events = events + pygame.event.get()
            if (currentTime - timeSinceKeyRegistered) > .01:

                for event in events:
                    if event.type == QUIT:
                        exit(0)
                timeSinceKeyRegistered = currentTime
                self.timeExecutive.executeTimers(currentTime)
                if not automation.screenIsLocked():
                    self.runUserInteractionChecks(events)

                automation.runActions()

                self.runEvents()

                self.world.refreshActiveMap()
                events = []


    def runUserInteractionChecks(self, events):
        commands = []
        mouse = pygame.mouse.get_pos()
        self.world.updateMousePosition(mouse)
        keys = pygame.key.get_pressed()
        commands = []
        for event in events:
            # if we got a keyup event
            if event.type == KEYUP:
                if event.key == K_SPACE:
                    commands.append(types.ACTION_SPACE_RELEASED)
                    refreshNeeded = True
                if keys[pygame.K_RSHIFT]:
                    commands.append(types.ACTION_RSHIFT_RELEASED)
                    refreshNeeded = True
                if event.key == K_e:
                    commands.append(types.ACTION_USE_BUTTON_RELEASED)
                    refreshNeeded = True
                if event.key == K_t:
                    self.world.doDebugAction()  # hook up commands to test to the 't' key
                    refreshNeeded = True
                if event.key == K_ESCAPE:
                    self.world.toggleMenu(types.MAP_NAME_ESC_MENU)
                    refreshNeeded = True
                if event.key == K_i:
                    self.world.toggleMenu(types.MAP_NAME_INVENTORY_MENU)
                    refreshNeeded = True
                if event.key == K_COMMA:
                    self.inventory.goBackwardInInventory()
                    refreshNeeded = True
                if event.key == K_PERIOD:
                    self.inventory.goForwardInInventory()
                    refreshNeeded = True
            if event.type == pygame.MOUSEBUTTONDOWN:
                x, y = event.pos
                print "CLICK! (" + str(x) + ", " + str(y) + ")"
                self.world.click(x, y, True)
            if event.type == pygame.MOUSEBUTTONUP:
                x, y = event.pos
                self.world.click(x, y, False)
                refreshNeeded = True

        if keys[pygame.K_w]:
            commands.append(types.DIRECTION_UP)
        if keys[pygame.K_a]:
            commands.append(types.DIRECTION_LEFT)
        if keys[pygame.K_s]:
            commands.append(types.DIRECTION_DOWN)
        if keys[pygame.K_d]:
            commands.append(types.DIRECTION_RIGHT)
        if keys[pygame.K_SPACE]:
            commands.append(types.ACTION_SPACE_PRESSED)
        if keys[pygame.K_RSHIFT]:
            commands.append(types.ACTION_RSHIFT_PRESSED)
        if keys[pygame.K_e]:
            commands.append(types.ACTION_USE_BUTTON_PRESSED)

        if commands:
            self.world.handleUserCommands(commands)


    def runEvents(self):
        if self.events:
            for e in self.events:
                event = e.split("(")[0]
                data = e.split("(")[1].split(")")[0]
                sender = e.split("(")[1].split(")")[1]

                if event.startswith(types.EVENT_TELEPORT_SPRITE):
                    x,y,destination,id = data.split()
                    self.world.teleportSprite(id,destination,(x,y))

                if event.startswith(types.EVENT_TELEPORT_ACTIVE_SPRITE):
                    
                    x,y,destination = data.split()
                    print ("teleporting active sprite! to...")
                    print (destination)
                    x = int(x)
                    y = int(y)
                    self.world.teleportActiveSprite(x,y,destination)

                if event.startswith(types.EVENT_SCORE):
                    print data

                if event.startswith(types.EVENT_DRAW_TEMPORARY_TEXT):
                    print data

                if event.startswith(types.EVENT_DELAYED_ERASE):
                    id, timeToErase = data.split("(")[1].split(")")[0].split()
                    self.timeExecutive.addTimer(
                        Timer(self.world.eraseElementOnRegion, (sender, id), time.time(), int(timeToErase)))

                if event.startswith(types.EVENT_DIALOGUE):
                    self.world.startDialogue(data)
                    refreshNeeded = True

                if event.startswith(types.EVENT_END_DIALOGUE):
                    self.world.endDialogue()


                # we're done processing that event, go ahead and remove it
                self.events.pop()

