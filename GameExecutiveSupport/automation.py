import sharedData.types as types
from sharedData.borg import *
import time
import random
import sharedData.utilities as utilities
from sharedData.EventHandler import *
from sharedData.sharedGameData import *
import commonsequences as commonSequences
import sprite.commonSprites as commonSprites

class Automation(Borg):
    def __init__(self, world):
        Borg.__init__(self)
        try:
            if self.initialized:
                return
        except:
            self.world = world
            self.lockedScreen = False
            self.timeOfDay = types.SEQUENTIAL_LIST_OF_TIMES[0]
            self.guests = []
            self.hospitalitySprites = []
            self.guestActionsForHour = dict()
            self.initialized = True
            self.eventhandler = eventHandler()
            self.sharedData = SharedGameMemory()
            self.timeSinceClockLastUpdated = time.time()
            self.subscribeForEvents()
            self.setupSequencesForGuests()
            self.lampOn = False
            self.paused = False
            self.screen = None
            self.hygieneSprites = []


    def subscribeForEvents(self):
        self.eventhandler.subscribeForEvent(types.EVENT_CLOCK_HOUR_ELAPSED, self.hourHasElapsed)
        self.eventhandler.subscribeForEvent(types.EVENT_CLOCK_PAUSED, self.pause)
        self.eventhandler.subscribeForEvent(types.EVENT_CLOCK_RESUME, self.unpause)
        self.eventhandler.subscribeForEvent(types.EVENT_LAMP_TOGGLE, self.lampToggle)
        self.eventhandler.subscribeForEvent(types.EVENT_GUEST_DONE_CHECKING_OUT, self.guestLeave)
        self.eventhandler.subscribeForEvent(types.EVENT_DELETE_SPRITE, self.checkForDeletedGuest)
        self.eventhandler.subscribeForEvent(types.EVENT_SLEEP, self.handleSleepEvent)


    def setScreen(self, screen):
        self.screen = screen

    def handleSleepEvent(self, data):
        self.fade(fadeIn=False)
        self.doSleepActions()
        self.fade(fadeIn=True)
        return


    def fade(self, fadeIn = True, alphaSurface = None):
        if fadeIn:
            alpha = 255
            alphaUnit = -10
            alphaFadeComplete = 0
        else:
            alpha = 0
            alphaUnit = 10
            alphaFadeComplete = 255
        done = False

        while not done:
            time.sleep(.01)
            self.eventhandler.queueRedrawScreenInBackgroundEvent("automation")
            alphaSurface = pygame.Surface((1024, 768))  # The custom-surface of the size of the screen.
            alphaSurface.fill(types.COLOR_TYPE_BLACK)  # Fill it with whole white before the main-loop.
            alphaSurface.set_alpha(alpha)  # Set alpha to 0 before the main-loop.
            alpha += alphaUnit
            self.screen.blit(alphaSurface, (0, 0))
            pygame.display.update()
            if fadeIn:
                if alpha <= alphaFadeComplete:
                    done = True
            else:
                if alpha >= alphaFadeComplete:
                    done = True
        return alphaSurface

    def doSleepActions(self):
        self.sharedData.updateScoresForDay()
        self.resetGuests()
        self.resetHospitalitySprites()
        self.resetHygieneSprites()
        pass

    def resetGuests(self):
        spritesToDelete = []+self.guests
        for g in spritesToDelete:
            self.eventhandler.queueDeleteSprite(g.id, "automation")
        upstairs = self.world.getMapByName(types.MAP_NAME_UPSTAIRS)
        guests = commonSprites.generateAllGuests()
        room = 0
        for g in guests:
            upstairs.addSprite(g)
            self.registerSpriteAsGuestByID(g.name, room)
            room+=1

    def checkForDeletedGuest(self, data):
        event = data.split("(")[0]
        guestID =data.split("(")[1].split(")")[0]
        sender = data.split("(")[1].split(")")[1]

        guest, index = self.getGuestByID(guestID)
        if guest:
            self.guests.pop(index)
            print "deleting" + guestID

    def getGuestByID(self, guestName):
        index = 0
        for g in self.guests:
            if guestName == g.id:
                return g, index
            index+=1

        print "can't find guest by name "+ guestName
        return None, -1

    def setupSequencesForGuests(self):
        self.guestActionsForHour[9] = [types.GUEST_GO_DOWNSTAIRS_FROM_ROOM, types.GUEST_GO_FROM_DOWNSTAIRS_TO_LOBBY]

    def registerSpriteAsGuestByID(self, spriteID, room):
        self.guests.append(Guest(spriteID, self.world, room))

    def registerSpriteAsHospitalityByID(self, spriteID):
        self.hospitalitySprites.append(spriteID)

    def registerSpriteAsHygieneSpriteByID(self, spriteID, room):
        self.hygieneSprites.append((spriteID, room))

    def resetHygieneSprites(self):
        nrOfGuests = len(self.guests)-1
        for h, room in self.hygieneSprites:
            if room <=nrOfGuests:
                self.eventhandler.queueSpriteReset(h, "automation")

    def lampToggle(self, data):
        if self.lampOn:
            self.lampOn = False
        else:
            self.lampOn = True
            self.guestsSeeLight()

        print self.lampOn

    def resetHospitalitySprites(self):
        for h in self.hospitalitySprites:
            self.eventhandler.queueSpriteReset(h, "automation")

    def pause(self, data):
        self.paused = True

    def unpause(self, data):
        self.paused = False

    def runActions(self):
        currentTime = time.time()
        if not self.paused:
            self.doGuestActions(currentTime)
            self.doNPCActions()
        self.updateClocks(currentTime)

    def updateClocks(self,currentTime):
        if currentTime - self.timeSinceClockLastUpdated >=1:
            self.sharedData.updateClock()
            self.timeSinceClockLastUpdated = currentTime

    def hourHasElapsed(self, data):
        event = data.split("(")[0]
        hour = int(data.split("(")[1].split(")")[0])
        sender = data.split("(")[1].split(")")[1]

        try:
            for g in self.guests:
                g.queueSequences(self.guestActionsForHour[hour])
                if not g.isSequenceRunning():
                    g.proceedToNextSequence()

        except:
            print "no actions found for hour"


    def doGuestActions(self, currentTime):
        for g in self.guests:
            g.runAction(currentTime)

    def doNPCActions(self):
        pass

    def screenIsLocked(self):
        return self.lockedScreen

    def guestsSeeLight(self):
        for g in self.guests:
            if g.idleAction == types.GUEST_BEHAVIOR_WAIT_IN_LOBBY:
                g.queueSequence(types.GUEST_LINEUP)
                g.proceedToNextSequence()
                self.eventhandler.queueGuestReadyForCheckout(g.id, "automation")

    def guestLeave(self, data):
        event = data.split("(")[0]
        guest = data.split("(")[1].split(")")[0]
        sender = data.split("(")[1].split(")")[1]

        for g in self.guests:
            print [g.id, guest]
            if g.id == guest:
                print guest
                g.queueSequence(types.GUEST_LEAVE)
                g.proceedToNextSequence()

class Guest:
    def __init__(self, spriteID, world, room):
        self.id = spriteID
        self.timeUntilNextAction = 0
        self.timeOfLastAction = 0
        self.eventhandler = eventHandler()
        self.activeSequence = False
        self.sequence = []
        self.endOfSequenceAction = None
        self.queuedSequences = []
        self.idleAction = None
        self.world = world
        self.room = room

    def runAction(self, currentTime):
        if self.timeToDoNextAction(currentTime):
            self.doAction()

    def timeToDoNextAction(self, currentTime):
        if self.activeSequence:
            self.timeOfLastAction = currentTime
            self.timeUntilNextAction = 0
            return True

        if (currentTime - self.timeOfLastAction) >= self.timeUntilNextAction:
            self.timeOfLastAction = currentTime
            self.timeUntilNextAction = random.random()
            return True

        else:
            return False

    def setActiveSequence(self, sequenceData):
        self.sequence, self.endOfSequenceAction, self.idleAction = sequenceData
        self.activeSequence = True

    def proceedToNextSequence(self):

        if self.queuedSequences:
            self.sequence, self.endOfSequenceAction, self.idleAction = self.queuedSequences.pop(0)

            self.activeSequence = True
        else:
            self.activeSequence = False
            self.idleAction = types.GUEST_IDLE_RANDOM_WALKING

    def queueSequence(self, sequenceName):

        mySequence = commonSequences.generateSequenceByName(sequenceName, self.room, self.id)
        self.queuedSequences.append(mySequence)

    def queueSequences(self, sequenceList):

        for s in sequenceList:
            self.queueSequence(s)

    def isSequenceRunning(self):
        return self.activeSequence

    def setIdleAction(self, idleAction):
        self.idleAction = idleAction

    def processActionComplete(self):

        method, args = self.endOfSequenceAction
        utilities.remoteExecuteFunction(method, args)
        if self.idleAction == types.GUEST_IDLE_NOACTION:
            self.proceedToNextSequence()
        else:

            self.activeSequence = False

    def doAction(self):

        if self.activeSequence:
            movementComplete = self.world.moveSpriteTowardsLocation(self.id, self.sequence[0])
            if movementComplete:
                self.sequence.pop(0)
            if len(self.sequence) <=0:
                self.processActionComplete()


            return

        if self.idleAction == types.GUEST_IDLE_RANDOM_WALKING:
                direction = utilities.getARandomDirection()
                self.world.moveSpriteByDirectionById(self.id, direction)

        if self.idleAction == types.GUEST_IDLE_BEHAVIOR_WAIT:
            pass#they need to stand still

        else:
            direction = utilities.getARandomDirection()
            self.world.moveSpriteByDirectionById(self.id, direction)


