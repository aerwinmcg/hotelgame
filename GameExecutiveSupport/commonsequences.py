import sharedData.types as types
from sharedData.EventHandler import *
import sharedData.utilities as utilities
import copy


def generateSequenceByName(name, guestRoom, guestName):
    if name == types.GUEST_GO_DOWNSTAIRS_FROM_ROOM:
        return roomToDownstairs(guestRoom, guestName)
    if name == types.GUEST_GO_FROM_DOWNSTAIRS_TO_LOBBY:
        return downstairsToLobbyIdleArea()
    if name == types.GUEST_LINEUP:
        return lineUp()
    if name == types.GUEST_LEAVE:
        return leave(guestName)

def roomToDownstairs(guestRoom, guestName, idle=types.GUEST_IDLE_NOACTION):
    eventhandler = eventHandler()

    movementSequence = list(reversed(types.SEQUENTIAL_LIST_OF_MOVEMENTS_BETWEEN_STAIRS_AND_ROOMS[guestRoom]))[1:]

    x, y = types.INSIDE_LOCATION_STAIRS
    method = eventhandler.queueSpriteTeleport
    args = [x, y, types.MAP_NAME_INTERIOR, guestName, "automation"]

    print "about to return seqence:"
    return [movementSequence, (method, args), idle]

def downstairsToLobbyIdleArea(idle=types.GUEST_BEHAVIOR_WAIT_IN_LOBBY):
    movementSequence = [utilities.generateRandomPointInRect(types.RECTANGLE_AROUND_GUEST_WAITING_AREA)]
    #movementSequence = copy.copy(types.MOVEMENT_SEQUENCE_STAIRS_TO_LOBBY_FREE_AREA)
    #movementSequence.append(types.MOVEMENT_SEQUENCE_STAIRS_TO_LOBBY_FREE_AREA)

    method = None
    args = None


    return [movementSequence, (method, args), idle]


def lineUp(idle=types.GUEST_IDLE_BEHAVIOR_WAIT):
    eventhandler = eventHandler()

    movementSequence = [types.INSIDE_LOCATION_IN_FRONT_OF_DESK]

    method = None
    args = None

    return [movementSequence, (method, args), idle]


def leave(guestID, idle=types.GUEST_IDLE_BEHAVIOR_WAIT):
    eventhandler = eventHandler()

    movementSequence = [types.INSIDE_LOCATION_FRONT_DOOR]

    method = eventhandler.queueDeleteSprite
    args = [guestID, "automation"]

    return [movementSequence, (method, args), idle]